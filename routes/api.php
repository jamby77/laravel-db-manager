<?php
$config = config('dbmanager.route_group_config');
$config['prefix'] = api_prefix();
$config['middleware'] = config('dbmanager.api.middleware');
Route::namespace($config['namespace'])
    ->middleware($config['middleware'])
    ->prefix($config['prefix'])
    ->group(function () use ($config) {
        Route::apiResource('/', 'TablesController', $config)
            ->names([
                'index' => 'dbmanager-api',
                'show' => 'dbmanager-api.show',
                'store' => 'dbmanager-api.store',
                'update' => 'dbmanager-api.update',
                'destroy' => 'dbmanager-api.destroy',
            ]);
        // Table Data
        Route::get('/{table}/data', 'TableDataController@index')->name('dbmanager.tables.data.index');
        Route::get('/{table}/data/{rowId}', 'TableDataController@show')->name('dbmanager.tables.data.read');
        Route::post('/{table}/data', 'TableDataController@store')->name('dbmanager.tables.data.create');
        Route::put('/{table}/data/{rowId}', 'TableDataController@update')->name('dbmanager.tables.data.update');
        Route::delete('/{table}/data/{rowId}', 'TableDataController@destroy')->name('dbmanager.tables.data.delete');
        Route::delete('/{table}', 'TableDataController@truncate')->name('dbmanager.tables.data.delete-all');
        // End Table Data

        // db config
        Route::get('/db-config', 'DbConfigController@get')->name('dbmanager.db-config.getAll');
        Route::get('/db-config/{table}', 'DbConfigController@get')->name('dbmanager.db-config.get');
        Route::post('/db-config', 'DbConfigController@update')->name('dbmanager.db-config.updateAll');
        Route::post('/db-config/{table}', 'DbConfigController@update')->name('dbmanager.db-config.update');
        // end db config
    });

