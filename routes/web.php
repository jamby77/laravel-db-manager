<?php

Route::group(config('dbmanager.route_group_config'), function () {
    Route::get('/', 'CrudController@index')->name('dbmanager');
    Route::get('{any}', 'CrudController@index')->where('any', '.*');
});
