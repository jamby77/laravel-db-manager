import React, { Component, createRef } from "react";
import cn from "classnames";
import axios from "axios";
import dayjs from "dayjs";
import { parse } from "query-string";
import isEmpty from "lodash/isEmpty";
import Sidebar from "./common/Sidebar";
import Crud from "./crud/Crud";
import {
  BrowserRouter as Router,
  matchPath,
  Route,
  withRouter
} from "react-router-dom";
import { AppStates } from "./AppState";
import { crudValue, Provider } from "./CrudContext";
import Error from "./common/Error";
import { actionsCol } from "../misc/helpers";
import PageLoader from "./common/PageLoader";

const mainClasses = [
  "main",
  "row",
  "h-100",
  "w-100",
  "flex-lg-nowrap",
  "flex-md-nowrap",
  "position-fixed"
];
let contextValue;
const tablePath = `/${window.laraPMA.basePath}/:table`;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { ...crudValue, ...props.appConfig };
    const match = matchPath(this.props.location.pathname, {
      path: tablePath
    });
    if (match) {
      this.state.table = match.params.table;
      const urlQuery = parse(this.props.location.search);
      const { page, pagesize, ...filters } = urlQuery;
      if (!isEmpty(page)) {
        this.state.pagination.page = page;
      }
      if (!isEmpty(pagesize)) {
        this.state.pagination.perPage = pagesize;
      }
      if (!isEmpty(filters)) {
        this.state.filters[this.state.table] = filters;
      }
    }
    this.el = createRef();
    this.loadingRef = createRef();
    this.bindHandlers();
  }

  bindHandlers() {
    this.fetchData.bind(this);
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    this.refreshDb = this.refreshDb.bind(this);
    this.loadTable = this.loadTable.bind(this);
    this.setFilters = this.setFilters.bind(this);
    this.setPagination = this.setPagination.bind(this);
    this.setActionsVisible = this.setActionsVisible.bind(this);
  }

  fetchTables() {
    return axios.get(this.allTablesUrl());
  }

  fetchTable(table) {
    return axios.get(this.tableUrl(table), {
      params: this.state.params(table, this.state)
    });
  }

  fetchData() {
    let requests = [this.fetchTables()];
    if (this.state.table) {
      requests.push(this.fetchTable(this.state.table));
    }
    axios
      .all(requests)
      .then(
        axios.spread((tables, activeTable) => {
          const state = {
            database: tables.data,
            tablesUpdatedAt: dayjs()
          };
          if (activeTable) {
            state.currentTable = activeTable.data;
          }
          state.state = AppStates.Loaded;
          this.setState(state);
        })
      )
      .catch(error => {
        this.setState({ state: AppStates.Error });
        console.log(error);
      })
      .then(() => {
        console.log("done");
      });
  }

  setDocumentTitle() {
    if (window && this.state.currentTable.name) {
      if (!this.origTitle) {
        this.origTitle = window.document.title;
      }
      window.document.title = `${this.origTitle} - ${
        this.state.currentTable.name
      }`;
    }
  }

  componentDidMount() {
    this.fetchData();
    this.setDocumentTitle();
    this.setWindowDimensions();
    this.addResizeListener();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.setDocumentTitle();
  }

  componentWillUnmount() {
    if (window && this.origTitle) {
      window.document.title = this.origTitle;
    }
    this.removeResizeListener();
  }

  allTablesUrl() {
    const { appConfig } = this.props;
    return `${appConfig.baseUrl}`;
  }

  tableUrl(table) {
    const { appConfig } = this.props;
    return `${appConfig.baseUrl}/${table}/data`;
  }

  loadTable(table) {
    this.setState(
      currentState => {
        const tableLoadState = {
          state: AppStates.Loading,
          pagination: { ...currentState.pagination }
        };
        if (currentState.currentTable.name !== table) {
          // if loading new table, reset page number
          tableLoadState.pagination.page = 1;
        }
        return tableLoadState;
      },
      () => {
        this.fetchTable(table)
          .then(response => {
            this.setState({
              state: AppStates.Loaded,
              currentTable: response.data
            });
          })
          .catch(error => {
            this.setState({ state: AppStates.Error });
            console.log(error);
          })
          .then(() => {
            console.log("done load table");
          });
      }
    );
  }

  setFilters(filters) {
    const { name: table } = this.state.currentTable;
    // set any url filters per table. Filters are going to be preserved per table
    this.setState(
      currentState => {
        debugger;
        const filters = { ...(currentState.filters[table] || {}), ...filters };
        return { filters: { [table]: filters } };
      },
      () => this.loadTable(table)
    );
  }

  setPagination(pagination) {
    //
    const { name: table } = this.state.currentTable;
    // set pagination params
    this.setState(
      currentState => {
        if (
          pagination.perPage &&
          pagination.perPage !== currentState.pagination.perPage
        ) {
          // when pagesize is changed, reset page.
          pagination.page = 1;
        }
        return { pagination: { ...currentState.pagination, ...pagination } };
      },
      () => this.loadTable(table)
    );
  }

  setSortOn = sortOn => {
    const { name: table } = this.state.currentTable;
    // set pagination params
    this.setState(
      currentState => {
        const tableSorts = currentState.sort_on[table] || {};
        const newSort = { ...currentState.sort_on };
        if (sortOn.sortdir === null) {
          newSort[table] = undefined;
        } else {
          newSort[table] = { ...tableSorts, ...sortOn };
        }
        return { sort_on: newSort };
      },
      () => this.loadTable(table)
    );
  };

  /**
   * @returns {string}
   */
  getTableRowKey() {
    if (this.state.currentTable["keyField"]) {
      return this.state.currentTable["keyField"];
    }
    if (this.state.currentTable["columns"]) {
      return this.state.currentTable["columns"].map(function(c) {
        return c.name !== actionsCol && c.name;
      });
    }
    return "";
  }

  async refreshDb() {
    console.log("Refreshing database data");
    const tables = await this.fetchTables();

    const state = {
      database: tables.data,
      tablesUpdatedAt: dayjs()
    };
    this.setState(state);
  }

  setWindowDimensions() {
    const w = window,
      d = document,
      e = d.documentElement,
      g = d.body || d.getElementsByTagName("body")[0],
      x = w.innerWidth || e.clientWidth || g.clientWidth,
      y = w.innerHeight || e.clientHeight || g.clientHeight,
      o = this.el.current.offsetTop;
    if (y - o > 0) {
      this.setState(state => {
        const sideBarWidth = state.sideBarWidth || 230;
        return {
          winWidth: x,
          winHeight: y,
          offsetTop: o,
          maxHeight: y - o,
          sideBarWidth,
          mainContentWidth: x - sideBarWidth
        };
      });
    }
  }

  updateWindowDimensions() {
    this.setWindowDimensions();
  }

  addResizeListener() {
    window.addEventListener("resize", this.updateWindowDimensions);
  }

  removeResizeListener() {
    window.removeEventListener("listener", this.updateWindowDimensions);
  }

  setActionsVisible({ right, width }) {
    if (!this.state.winWidth) {
      // cannot really do anything without window width
      return;
    }
    const { winWidth } = this.state;
    const x = winWidth + width - right;
    if (x < 0 && Math.abs(x) >= width / 2) {
      this.setState({
        actionsLong: true,
        rightPos: this.state.winWidth - width
      });
    } else {
      this.setState({ actionsLong: false });
    }
  }

  fetchRow = (table, id) => {
    return axios.get(`${this.tableUrl(table)}/${id}`);
  };

  createRecord = (table, record) => {
    return axios.post(`${this.tableUrl(table)}`, record);
  };

  updateRecord = (table, id, record) => {
    return axios.put(`${this.tableUrl(table)}/${id}`, record);
  };

  deleteRecord = (table, id) => {
    return axios.delete(`${this.tableUrl(table)}/${id}`).then(response => {
      if (response.status === 200) {
        this.loadTable(table);
      }
      return response;
    });
  };

  deleteAll = table => {
    return axios.delete(`${this.tableUrl(table)}`);
  };

  toggleFilters = () => {
    this.setState(state => ({ enableFilters: !state.enableFilters }));
  };

  resizeSideBar = x => {
    this.setState(state => {
      if (x < state.minSideBarWidth || x === state.sideBarWidth) {
        return null;
      }
      return { sideBarWidth: x, mainContentWidth: state.winWidth - x };
    });
  };

  toggleSidebar = () => {
    this.setState(state => {
      const w = state.sideBarWidth;
      let wc = 72;
      const newState = {
        showSidebar: !state.showSidebar
      };
      if (!newState.showSidebar) {
        newState.oldSBW = w;
      } else {
        wc = state.oldSBW;
      }

      newState.sideBarWidth = wc;
      newState.mainContentWidth = state.winWidth - wc;
      return newState;
    });
  };

  render() {
    const state = this.state.state;
    contextValue = {
      ...crudValue,
      ...this.state,
      refreshTables: this.refreshDb,
      loadTable: this.loadTable,
      setFilters: this.setFilters,
      fetchRow: this.fetchRow,
      createRecord: this.createRecord,
      updateRecord: this.updateRecord,
      deleteRecord: this.deleteRecord,
      setPagination: this.setPagination,
      setSortOn: this.setSortOn,
      setActionsVisible: this.setActionsVisible,
      toggleFilters: this.toggleFilters,
      resize: this.resizeSideBar,
      toggleSidebar: this.toggleSidebar
    };
    const loading = state === AppStates.Loading || state === AppStates.Initial;
    return (
      <Provider value={contextValue}>
        <Error>
          <div className={cn(["container-fluid", "h-100"])} ref={this.el}>
            {state === AppStates.Error && (
              <div className={cn("error", "has-error")}>
                Something Went wrong
              </div>
            )}
            <Router>
              <div className={cn(mainClasses)}>
                {loading && <PageLoader cancelRef={this.loadingRef} />}
                <Route path="/crud">{props => <Sidebar {...props} />}</Route>
                <Route path={tablePath}>
                  {() => {
                    return <Crud rowKey={this.getTableRowKey()} />;
                  }}
                </Route>
              </div>
            </Router>
          </div>
        </Error>
      </Provider>
    );
  }
}

export default withRouter(App);
