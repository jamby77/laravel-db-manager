import React, { Fragment } from "react";
import cn from "classnames";
import { Consumer } from "../CrudContext";
import { AppStates } from "../AppState";
import Error from "../common/Error";
import CrudHeader from "./CrudHeader";
import Table from "./Table";
import PaginationController from "../common/PaginationController";

const mainContentClasses = ["text-dark", "bg-light", "crud-container"];
const currentTableClasses = ["table", "table-striped", "dbmanager-grid"];

const Crud = ({ rowKey }) => {
  return (
    <Error>
      <Consumer>
        {context => {
          const {
            currentTable: table,
            state,
            maxHeight,
            mainContentWidth,
            pagerHeight
          } = context;
          if (!table.name || table.name === "") {
            // no table
            return (
              <h1
                style={{
                  margin: "30vh auto"
                }}
              >
                Click on a table in the menu to see its contents
              </h1>
            );
          }
          const loading =
            state === AppStates.Loading || state === AppStates.Initial;
          const styles = maxHeight
            ? {
                height: `${maxHeight}px`,
                width: `${mainContentWidth}px`,
                position: "absolute",
                bottom: "0px",
                top: "0px",
                left: "auto",
                right: "0px"
              }
            : {};
          const long = context.actionsLong;
          const gridHeight = maxHeight - (pagerHeight * 1.5 + pagerHeight);
          return (
            <section className={cn(mainContentClasses)} style={styles}>
              <div style={{ maxHeight: `${pagerHeight * 1.5}px` }}>
                <CrudHeader table={table ? table.name : "Main"} />
              </div>
              <div>
                {table ? (
                  <Fragment>
                    <Table
                      long={long}
                      loading={loading}
                      tableClasses={cn(currentTableClasses)}
                      table={table}
                      rowKey={rowKey}
                      containerClasses={cn(["table-container", "p-lg-3"])}
                      containerStyle={{ height: `${gridHeight}px` }}
                    />

                    <div
                      className="pagination-container"
                      style={{ maxHeight: `${pagerHeight}px` }}
                    >
                      <PaginationController />
                    </div>
                  </Fragment>
                ) : (
                  <h1 className={cn(["display-1"])}>"No table selected"</h1>
                )}
              </div>
            </section>
          );
        }}
      </Consumer>
    </Error>
  );
};

export default Crud;
