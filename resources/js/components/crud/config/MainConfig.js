import React, { Component } from "react";
import PropTypes from "prop-types";
import { Form, Formik } from "formik";
import { inputType } from "../../../misc/forms";
import { actionsCol } from "../../../misc/helpers";
import ConfigItem from "./ConfigItem";

class MainConfig extends Component {
  render() {
    const { columns } = this.props.table;
    const {
      submitBinder,
      resetBinder,
      handleSubmit = (values, actions) => console.log(values, actions)
    } = this.props;

    return (
      <div>
        <Formik initialValues={{ columns }} onSubmit={handleSubmit}>
          {formProps => {
            if (submitBinder) {
              submitBinder(formProps.handleSubmit);
            }
            if (resetBinder) {
              resetBinder(formProps.resetForm);
            }
            return (
              <Form
                style={{
                  display: "flex",
                  flexWrap: "wrap"
                }}
              >
                {formProps.values.columns.map((a, i) => {
                  const prefixId = `columns.${i}`;
                  const id = `${prefixId}.${a.field}`;
                  const fieldProps = {
                    name: id,
                    className: `form-control`,
                    containerClass: "col"
                  };
                  if (!a.inputType) {
                    a.inputType = inputType(a);
                  }
                  return (
                    <ConfigItem
                      column={a}
                      id={id}
                      isActions={a.field === actionsCol}
                      prefixId={prefixId}
                      fieldProps={fieldProps}
                      style={{
                        borderBottom: "1px solid #ccc",
                        marginBottom: "1rem",
                        minWidth: "32%",
                        maxWidth: "100%",
                        marginLeft: "1rem"
                      }}
                    />
                  );
                })}
              </Form>
            );
          }}
        </Formik>
      </div>
    );
  }
}

MainConfig.propTypes = {
  table: PropTypes.object
};

export default MainConfig;
