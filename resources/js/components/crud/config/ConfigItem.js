import React from "react";
import { Field } from "formik";
import InputText from "../forms/InputText";
import ControlPlaintext from "../forms/ControlPlaintext";
import InputSelect from "../forms/InputSelect";
import { prepareSelectOptions, supportedInputTypes } from "../../../misc/forms";
import InputCheckbox from "../forms/InputCheckbox";

const ConfigItem = ({ column, fieldProps, id, prefixId, isActions, style }) => {
  return (
    <div key={id} style={style}>
      <div className="form-group">
        <Field
          {...fieldProps}
          component={InputText}
          name={`${prefixId}.field`}
          label="Field"
          key={`${id}.field`}
          readOnly={true}
        />
      </div>
      {!isActions && (
        <div className="form-group">
          <Field
            {...fieldProps}
            component={InputText}
            name={`${prefixId}.label`}
            placeholder="Label"
            label="Label"
            key={`${id}.label`}
            value={column.label}
          />
        </div>
      )}
      {!isActions && (
        <div className="form-group">
          <Field
            {...fieldProps}
            component={
              column.inputType === "readonly" ? ControlPlaintext : InputSelect
            }
            name={`${prefixId}.inputType`}
            label="Input type"
            key={`${id}.inputType`}
            value={column.inputType}
            options={prepareSelectOptions(supportedInputTypes)}
          />
        </div>
      )}
      <Field
        {...fieldProps}
        component={InputCheckbox}
        name={`${prefixId}.visible`}
        label="Show field"
        key={`${id}.visible`}
        value={column.visible}
      />
      {!isActions && [
        <Field
          {...fieldProps}
          component={InputCheckbox}
          name={`${prefixId}.filterable`}
          label="Filter by field"
          key={`${id}.filterable`}
          value={column.filterable}
        />,
        <Field
          {...fieldProps}
          component={InputCheckbox}
          name={`${prefixId}.sortable`}
          label="Sort by field"
          key={`${id}.sortable`}
          value={column.sortable}
        />
      ]}
    </div>
  );
};

export default ConfigItem;
