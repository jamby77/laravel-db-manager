import React, { Component } from "react";
import PropTypes from "prop-types";

import { actionsCol, getCellKey, getCellValue } from "../../misc/helpers";
import cn from "classnames";
import TableActionCell from "./TableActionCell";
import Cell from "./Cell";

class Row extends Component {
  constructor(props) {
    super(props);
    this.rowRef = React.createRef();
    this.state = { hover: false };
  }

  componentDidMount() {
    const rect = this.rowRef.current.getBoundingClientRect();
    this.setState({ initialRect: rect, rect });
  }

  render() {
    const { long, keyField, row, columns } = this.props;
    const topOffset = this.state.initialRect
      ? this.state.initialRect.top - this.state.rect.top
      : 0;
    return (
      <tr
        ref={this.rowRef}
        className={cn({ long })}
        onMouseEnter={() => {
          if (!long) {
            return;
          }
          this.setState({
            hover: true,
            rect: this.rowRef.current.getBoundingClientRect()
          });
        }}
        onMouseLeave={() => {
          if (!long) {
            return;
          }
          this.setState({
            hover: false
          });
        }}
      >
        {columns.map(col =>
          col.field === actionsCol ? (
            <TableActionCell
              hovered={this.state.hover}
              topOffset={topOffset}
              style={col.style}
              config={row[col.field]}
              key={getCellKey(row, keyField, col)}
            />
          ) : (
            <Cell style={col.style} key={getCellKey(row, keyField, col)}>
              {getCellValue(row, col)}
            </Cell>
          )
        )}
      </tr>
    );
  }
}

export default Row;

Row.propTypes = {
  keyField: PropTypes.any,
  long: PropTypes.bool,
  row: PropTypes.object.isRequired,
  columns: PropTypes.array.isRequired
};
