import React from "react";
import TableHead from "./TableHead";
import TableBody from "./TableBody";
import Error from "../common/Error";

const Table = props => {
  const {
    table = {},
    rowKey,
    containerStyle = {},
    containerClasses = "",
    tableClasses = "table table-striped",
    long = false,
    loading = false
  } = props;
  console.log(props);
  return (
    <Error>
      <div className={containerClasses} style={containerStyle}>
        {table.columns && table.data ? (
          <table key="current-table" className={tableClasses}>
            {table.columns && <TableHead columns={table.columns} />}
            {table.data && (
              <TableBody
                long={long}
                loading={loading}
                columns={table.columns}
                data={table.data}
                keyField={rowKey}
              />
            )}
          </table>
        ) : (
          <h1>Table data is missing</h1>
        )}
      </div>
    </Error>
  );
};

export default Table;
