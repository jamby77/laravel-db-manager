import React from "react";
import CommonAction from "./CommonAction";
import { Consumer } from "../../CrudContext";
import { actionsCol, getCellValue, findRow } from "../../../misc/helpers";

const ViewAction = ({ id, row, ...props }) => {
  return (
    row && (
      <Consumer>
        {context => {
          const { currentTable: table } = context;
          const { data, keyField } = table;
          return (
            <CommonAction
              {...props}
              title={`Details for record ${id}`}
              confirm={false}
            >
              {row && (
                <table className={`table table-striped`}>
                  <thead>
                    <tr>
                      <th>Field</th>
                      <th>Value</th>
                    </tr>
                  </thead>
                  <tbody>
                    {Object.keys(row).map(field => {
                      return (
                        field !== actionsCol && (
                          <tr key={`${table.name}-${id}-${field}`}>
                            <td>{field}</td>
                            <td>{getCellValue(row, { field })}</td>
                          </tr>
                        )
                      );
                    })}
                  </tbody>
                </table>
              )}
            </CommonAction>
          );
        }}
      </Consumer>
    )
  );
};

export default ViewAction;
