import React from "react";
import CommonAction from "./CommonAction";

const CommentAction = ({ id, ...props }) => {
  return (
    <CommonAction {...props} title={`Comment entry`}>
      <h1>Comment on {id}</h1>
    </CommonAction>
  );
};

export default CommentAction;
