import React from "react";
import CommonAction from "./CommonAction";
import { Consumer } from "../../CrudContext";
import EntityForm from "../forms/EntityForm";
import { prepareFormConfig } from "../../../misc/forms";

const EditAction = ({ id, row, errors, errorMessage, onConfirm, ...props }) => {
  let submitHandler;
  return (
    row && (
      <Consumer>
        {context => {
          const { currentTable: table } = context;
          const { columns } = table;
          for (let f in row) {
            if (row[f] === null) {
              row[f] = undefined;
            }
          }
          return (
            <CommonAction
              {...props}
              message={errorMessage}
              messageType="error"
              title={`Edit ${id}`}
              onConfirm={e => {
                if (submitHandler) {
                  submitHandler(e);
                } else {
                  console.log("Submit binding failed");
                }
              }}
            >
              <EntityForm
                handleSubmit={(values, actions) => {
                  // console.log(values, actions);
                  onConfirm(values);
                }}
                submitBinder={fn => {
                  submitHandler = fn;
                }}
                errors={errors}
                entity={row}
                formConfig={prepareFormConfig(row, columns)}
                // validationSchema={prepareValidationSchema(columns)}
              />
            </CommonAction>
          );
        }}
      </Consumer>
    )
  );
};

export default EditAction;
