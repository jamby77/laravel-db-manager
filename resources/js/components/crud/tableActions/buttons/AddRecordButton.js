import React, { PureComponent, createRef } from "react";
import { CrudContext } from "../../../CrudContext";
import { actionsCol, isEmpty } from "../../../../misc/helpers";
import CreateAction from "../CreateAction";

const initialState = {
  addingRecord: false,
  hasError: false,
  errors: [],
  errorMessage: ""
};

class AddRecordButton extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      ...initialState
    };
    this.closeBtnRef = createRef();
  }

  closeActionDialog = () => {
    this.setState({ ...initialState });
  };

  addRecord = () => {
    const { currentTable: table } = this.context;
    const columns = table.columns;
    const newRecord = {};
    columns.forEach(col => {
      if (col.field === actionsCol || col.autoincrement === true) {
        return;
      }
      newRecord[col.field] = isEmpty(col.default) ? undefined : col.default;
    });
    this.setState({ addingRecord: true, row: newRecord });
  };
  renderActionDialog = () => {
    const actionProps = {
      onDismiss: this.closeActionDialog,
      onConfirm: this.submitRecord,
      focusRef: this.closeBtnRef,
      ...this.state
    };

    return <CreateAction {...actionProps} />;
  };

  submitRecord = record => {
    console.log(record);
    const { createRecord, currentTable } = this.context;
    createRecord(currentTable.name, record)
      .then(response => {
        if (response.status === 200) {
          this.closeActionDialog();
          this.context.loadTable(currentTable.name);
        }
      })
      .catch(({ response }) => {
        const errors = {};
        if (response.data.errors) {
          for (let field in response.data.errors) {
            errors[field] = response.data.errors[field].join("\n");
          }
        }
        this.setState({
          hasError: true,
          errors,
          errorMessage: response.data.message
        });
      });
  };

  render() {
    return (
      <div>
        <button className={`btn btn-success`} onClick={this.addRecord}>
          Add Record
        </button>
        {this.state.addingRecord && this.renderActionDialog()}
      </div>
    );
  }
}
AddRecordButton.contextType = CrudContext;
export default AddRecordButton;
