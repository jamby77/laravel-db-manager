import React from "react";
import { Consumer } from "../../../CrudContext";

const ToggleFiltersButton = () => {
  return (
    <div className="toggle-filters-button">
      <Consumer>
        {context => {
          return (
            <button
              className={`btn btn-default`}
              onClick={() => {
                context.toggleFilters && context.toggleFilters();
              }}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 30 30"
                className={context.enableFilters ? "enabled" : "disabled"}
              >
                <path d="M 4 4 A 1.0001 1.0001 0 1 0 4 6 L 4.8007812 6 L 12 15 L 18 15 L 25.199219 6 L 26 6 A 1.0001 1.0001 0 1 0 26 4 L 4 4 z M 12 17 L 12 26 L 18 24 L 18 17 L 12 17 z" />
              </svg>
              {context.enableFilters ? "Disable" : "Enable"} Filters
            </button>
          );
        }}
      </Consumer>
    </div>
  );
};

export default ToggleFiltersButton;
