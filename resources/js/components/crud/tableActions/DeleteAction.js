import React from "react";
import {
  AlertDialog,
  AlertDialogDescription,
  AlertDialogLabel
} from "@reach/alert-dialog";

const DeleteAction = ({ id, onDismiss, onConfirm, focusRef }) => {
  return (
    <AlertDialog
      leastDestructiveRef={focusRef}
      className="modal-dialog"
      role="document"
    >
      <div className="modal-content">
        <div className="modal-header">
          <AlertDialogLabel>Please Confirm!</AlertDialogLabel>
        </div>
        <div className="modal-body">
          <AlertDialogDescription>
            <div className={`alert alert-danger`} role="alert">
              <p>
                Are you sure you want to delete record <strong>{id}</strong>?
              </p>
              <p>
                This action is permanent, and we're totally not just flipping a
                field called "deleted" to "true" in our database, we're actually
                deleting something.
              </p>
            </div>
          </AlertDialogDescription>
        </div>
        <div className="modal-footer">
          <div className="alert-buttons">
            <button onClick={onConfirm} className={`btn btn-outline-danger`}>
              Yes, delete
            </button>{" "}
            <button
              ref={focusRef}
              onClick={onDismiss}
              className={`btn btn-secondary`}
            >
              Cancel
            </button>
          </div>
        </div>
      </div>
    </AlertDialog>
  );
};

export default DeleteAction;
