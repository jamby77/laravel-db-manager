import React from "react";
import { DialogContent, DialogOverlay } from "@reach/dialog";
import VisuallyHidden from "@reach/visually-hidden";
import "@reach/dialog/styles.css";
import * as classnames from "classnames";
import AlertMessage from "../../common/AlertMessage";
import { Octicons } from "octicons-react/es/main";
import Octicon from "octicons-react/es/Octicon";

const CommonAction = ({
  id,
  onDismiss,
  onConfirm,
  children,
  focusRef,
  message,
  messageType,
  title = "Modal title",
  closeLbl = "Close",
  close = true,
  confirm = true,
  confirmLbl = "Save",
  confirmClass = "btn-primary",
  loading
}) => {
  return (
    <DialogOverlay onDismiss={onDismiss} initialFocusRef={focusRef}>
      <DialogContent role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">{title}</h5>
            {loading && (
              <Octicon icon={Octicons.sync} className={"loading"} scale={1.5} />
            )}
            <button
              type="button"
              className="close"
              onClick={onDismiss}
              aria-label="Close"
            >
              <VisuallyHidden>{closeLbl}</VisuallyHidden>
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            {message && <AlertMessage type={messageType} message={message} />}
            {children}
          </div>
          <div className="modal-footer">
            {close && (
              <button
                ref={focusRef}
                type="button"
                className="btn btn-secondary"
                onClick={onDismiss}
              >
                {closeLbl}
              </button>
            )}
            {confirm && (
              <button
                type="button"
                className={classnames("btn", confirmClass)}
                onClick={onConfirm}
              >
                {confirmLbl}
              </button>
            )}
          </div>
        </div>
      </DialogContent>
    </DialogOverlay>
  );
};

export default CommonAction;
