import React from "react";

const Thead = ({ children, className }) => {
  return <thead className={className}>{children}</thead>;
};

export default Thead;
