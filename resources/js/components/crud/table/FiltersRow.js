import React from "react";
import TableRow from "./TableRow";
import HeaderCell from "../HeaderCell";
import ColumnFilter from "../filters/ColumnFilter";
import { filterType } from "../../../misc/forms";

const FiltersRow = ({ columns }) => {
  return (
    <TableRow>
      {columns.map(col =>
        col.filterable ? (
          <HeaderCell config={{ sortable: false }}>
            <ColumnFilter
              name={col.field}
              placeholder={`Filter by ${col.name}`}
              type={filterType(col)}
            />
          </HeaderCell>
        ) : (
          <HeaderCell config={col}>{null}</HeaderCell>
        )
      )}
    </TableRow>
  );
};

export default FiltersRow;
