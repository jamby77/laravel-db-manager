import PropTypes from "prop-types";
import React, { Component } from "react";
import { Octicon, Octicons } from "octicons-react/es/main";
import { Link } from "react-router-dom";
import { stringify } from "query-string";
const directions = {
  a: "asc",
  d: "desc",
  n: null
};
class HeaderCell extends Component {
  constructor(props) {
    super(props);
    this.state = {
      direction: this.parseDirection(props.urlParams)
    };
  }

  parseDirection = urlParams => {
    if (
      urlParams &&
      this.props.config &&
      urlParams.sorton &&
      urlParams.sortdir &&
      urlParams.sorton === this.props.config.field
    ) {
      if (urlParams.sortdir === directions.a) {
        return directions.a;
      }
      if (urlParams.sortdir === directions.d) {
        return directions.d;
      }
    }
    return directions.n;
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    const direction = this.parseDirection(this.props.urlParams);
    if (direction === prevState.direction) {
      return;
    }
    this.setState({
      direction: direction
    });
  }

  componentDidMount() {
    const direction = this.parseDirection(this.props.urlParams);
    this.setState({
      direction: direction
    });
  }

  setSortOn() {
    const sort = { sorton: this.props.config.field };
    // cycle directions, asc to desc to neutral to asc
    const direction = this.state.direction;
    switch (direction) {
      case directions.a:
        sort.sortdir = directions.d;
        break;
      case directions.d:
        sort.sortdir = directions.n;
        break;
      default:
        sort.sortdir = directions.a;
        break;
    }

    this.setState(
      { direction: sort.sortdir },
      () => this.props.onSort && this.props.onSort(sort)
    );
  }

  render() {
    const { config, children, urlParams, onSort, url, ...rest } = this.props;
    if (!config) {
      return null;
    }
    return (
      <th {...rest}>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "flex-start"
          }}
        >
          <div className="th-title">{children}</div>
          {config && config.sortable && (
            <div className="th-sort-by">
              <Link
                onClick={this.setSortOn.bind(this)}
                className="page-link"
                to={{
                  pathname: url,
                  search: stringify(urlParams)
                }}
              >
                <Octicon
                  icon={
                    this.state.direction === directions.n
                      ? Octicons.dash
                      : this.state.direction === directions.a
                      ? Octicons.triangleUp
                      : Octicons.triangleDown
                  }
                />
              </Link>
            </div>
          )}
        </div>
      </th>
    );
  }
}

export default HeaderCell;

HeaderCell.propTypes = {
  children: PropTypes.any,
  config: PropTypes.object,
  onSort: PropTypes.func,
  url: PropTypes.string,
  urlParams: PropTypes.object
};
