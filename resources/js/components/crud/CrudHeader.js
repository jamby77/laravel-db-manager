import React from "react";
import { Octicon, Octicons } from "octicons-react/es/main";
import AddRecordButton from "./tableActions/buttons/AddRecordButton";
import ToggleFiltersButton from "./tableActions/buttons/ToggleFiltersButton";

const CrudHeader = ({ table }) => {
  return (
    <header>
      <h1 className={`crud-header`}>
        {table}{" "}
        {/* <button className="btn btn-link text-dark">
                    <Octicon icon={Octicons.info} scale={2} />
                </button>*/}
      </h1>
      <div className="crud-actions mx-3 py-3 border-top d-flex justify-content-between">
        <div className={`crud-filter`}>{null}</div>
        <div className="crud-action-buttons">
          <ToggleFiltersButton />
          <AddRecordButton />
        </div>
      </div>
    </header>
  );
};

export default CrudHeader;
