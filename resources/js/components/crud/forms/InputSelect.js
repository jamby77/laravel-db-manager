import React from "react";
import cn from "classnames";
import { ErrorMessage } from "formik";
import BaseSelect from "./BaseSelect";
import MultiSelect from "./MultiSelect";
import { isScalar, isEmpty } from "../../../misc/helpers";
import { prepareSelectOptions } from "../../../misc/forms";

const InputSelect = ({ field, form, options, ...props }) => {
  const { name, value } = field;
  const { label = name, containerClass, ...rest } = props;
  let selectValue = value;
  let handleChange = evt => {
    if (null === evt || undefined === evt || "" === evt) {
      form.setFieldValue(name, "");
      return;
    }
    if (typeof evt === "string") {
      // direct selection
      form.setFieldValue(name, evt);
      return;
    }
    if (evt.id && evt.text) {
      form.setFieldValue(name, evt.id);
      return;
    }
    let selectedOptions;
    if (Array.isArray(evt)) {
      selectedOptions = evt;
    }
    if (
      evt.target &&
      evt.target.selectedOptions &&
      Array.isArray(evt.target.selectedOptions)
    ) {
      selectedOptions = evt.target.selectedOptions;
    }
    if (selectedOptions && selectedOptions.length) {
      selectedOptions = [].slice.call(selectedOptions).map(option => option.id);
      form.setFieldValue(name, selectedOptions);
      return;
    }
    return field.onChange(evt);
  };
  if (props.multiple) {
    if (isScalar(value)) {
      selectValue = [value];
    } else if (isEmpty(value)) {
      selectValue = [];
    }
    selectValue = prepareSelectOptions(selectValue);
  }

  return (
    <div className={cn("form-group", containerClass)}>
      {props.multiple ? (
        <MultiSelect
          label={label}
          items={options}
          values={selectValue}
          handleChange={handleChange}
        />
      ) : (
        <BaseSelect
          {...field}
          handleChange={handleChange}
          id={name}
          value={selectValue}
          items={options}
          label={label}
          placeholder={"-- Please Select --"}
        />
      )}
      <ErrorMessage name={name} className={"field-error"} />
    </div>
  );
};

export default InputSelect;
