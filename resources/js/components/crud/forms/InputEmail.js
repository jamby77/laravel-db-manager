import React from "react";
import cn from "classnames";
import { ErrorMessage } from "formik";

const InputEmail = ({ field, form, ...props }) => {
  const { name } = field;
  const { label = name, containerClass, ...rest } = props;
  return (
    <div className={cn("form-group", containerClass)}>
      <label htmlFor={name}>{label}</label>
      <input {...rest} {...field} id={name} type="email" />
      <ErrorMessage name={name} className={"field-error"} />
    </div>
  );
};

export default InputEmail;
