import React from "react";
import cn from "classnames";

const Item = ({
  getItemProps = () => {
    throw "getItemProps is required";
  },
  active = false,
  listItem = "",
  itemAsString = "",
  itemStyle = {},
  index = 0
}) => {
  return (
    <li
      {...getItemProps({
        key: itemAsString,
        index,
        style: itemStyle,
        item: listItem,
        className: cn("base-select-menu-item", {
          active
        })
      })}
    >
      {itemAsString}
    </li>
  );
};

export default Item;
