import cn from "classnames";
import PropTypes from "prop-types";
import React from "react";
import { Octicon, Octicons } from "octicons-react";
import ClearSelectionButton from "./ClearSelectionButton";

export const ToggleButton = ({
  buttonProps,
  clearSelection,
  selectedItem = null,
  isOpen = false
}) => {
  const classNames = cn("btn", "btn-autocomplete");
  return (
    <React.Fragment>
      {selectedItem && (
        <ClearSelectionButton onClick={clearSelection} className={classNames} />
      )}
      <button className={classNames} {...buttonProps()}>
        <Octicon icon={isOpen ? Octicons.chevronUp : Octicons.chevronDown} />
      </button>
    </React.Fragment>
  );
};

ToggleButton.propTypes = {
  buttonProps: PropTypes.func,
  clearSelection: PropTypes.func,
  isOpen: PropTypes.bool,
  selectedItem: PropTypes.any
};
