import React from "react";
import cn from "classnames";
import { Octicon, Octicons } from "octicons-react";

const ClearSelectionButton = ({ onClick, className }) => {
  return (
    <button
      className={cn(className, "btn-sm")}
      onClick={onClick}
      aria-label="clear selection"
    >
      <Octicon icon={Octicons.x} />
    </button>
  );
};

export default ClearSelectionButton;
