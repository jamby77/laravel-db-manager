import React, { Component } from "react";
import PropTypes from "prop-types";
import { ToggleButton } from "./ToggleButton";
import MultiSelectionItem from "./MultiSelectionItem";
import MultiSelectionAddon from "./MultiSelectionAddon";

class MultiSelectSelectionFormInput extends Component {
  constructor(props) {
    super(props);
    this.selectionContainerRef = React.createRef();
    this.selectionButtonsRef = React.createRef();
    this.selectionAddonRef = React.createRef();
    this.selectionRefs = {};
    const { items } = this.props;
    this.state = {
      selection: this.prepareSelection(items),
      hiddenItems: []
    };
  }

  componentDidMount() {
    this.calculateVisible();
  }

  calculateVisible = () => {
    const container = this.selectionContainerRef.current;
    if (!container) {
      return true;
    }
    let visibleItemsWidth = 0;
    const hiddenItems = [];
    const ids = Object.keys(this.selectionRefs);
    ids.forEach((id, i) => {
      const sr = this.selectionRefs[id];
      if (!sr.current) {
        return;
      }
      if (i === 0) {
        // first item never hidden
        visibleItemsWidth += sr.current.clientWidth;
        return;
      }
      if (visibleItemsWidth + sr.current.clientWidth > container.clientWidth) {
        // should be hidden
        hiddenItems.push(id);
      } else {
        visibleItemsWidth += sr.current.clientWidth;
      }
    });
    this.setState({ hiddenItems });
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.items !== this.props.items) {
      this.setState({
        selection: this.prepareSelection(this.props.items),
        hiddenItems: []
      });
      this.calculateVisible();
    }
  }

  prepareSelection = items => {
    return items.map(i => {
      let item = i;
      if (typeof i === "string") {
        item = {
          id: i,
          text: i
        };
      }
      if (!item.hasOwnProperty("id")) {
        throw "Select item must be a string or an object with 'id' field";
      }
      if (!this.selectionRefs[item.id]) {
        this.selectionRefs[item.id] = React.createRef();
      }
      return item;
    });
  };

  render() {
    const {
      isOpen,
      onSelectedItemClick,
      onSelectionAddonClick,
      getToggleButtonProps,
      clearSelection,
      placeholder
    } = this.props;
    const { selection: items } = this.state;
    return (
      <div className="multi-select-selection-row input-group">
        <div
          className="multi-select-selection-container input-group-append"
          ref={this.selectionContainerRef}
        >
          {items.length > 0 ? (
            items.map(i => (
              <MultiSelectionItem
                key={i.id}
                id={i.id}
                label={i.text}
                onClick={onSelectedItemClick}
                innerRef={this.selectionRefs[i.id]}
                isVisible={!this.state.hiddenItems.includes(i.id.toString())}
              />
            ))
          ) : (
            <p className="placeholder">{placeholder}</p>
          )}
        </div>
        {this.state.hiddenItems.length > 0 && (
          <MultiSelectionAddon
            numItems={items.length}
            onClick={onSelectionAddonClick}
            innerRef={this.selectionAddonRef}
          />
        )}
        <div
          className="multi-select-selection-buttons input-group-append"
          ref={this.selectionButtonsRef}
        >
          <ToggleButton
            selectedItem={items.length > 0}
            isOpen={isOpen}
            buttonProps={getToggleButtonProps}
            clearSelection={clearSelection}
          />
        </div>
      </div>
    );
  }
}
MultiSelectSelectionFormInput.defaultProps = {
  items: [],
  isOpen: false,
  onSelectedItemClick: id => console.log(id),
  onSelectionAddonClick: () => console.warn("Addon click handler not passed"),
  getToggleButtonProps: () => {},
  clearSelection: () =>
    console.warn("Clear selection click handler not passed"),
  placeholder: "Select"
};

MultiSelectSelectionFormInput.propTypes = {
  clearSelection: PropTypes.func,
  getToggleButtonProps: PropTypes.func,
  isOpen: PropTypes.bool,
  items: PropTypes.array,
  onSelectedItemClick: PropTypes.func,
  onSelectionAddonClick: PropTypes.func,
  placeholder: PropTypes.string
};

export default MultiSelectSelectionFormInput;
