import React from "react";

const MultiSelectionAddon = ({ numItems = 0, onClick, innerRef = null }) => {
  return (
    numItems > 0 && (
      <div
        ref={innerRef}
        className="multi-select-selection-addon input-group-append"
        onClick={onClick}
      >
        <span className="multi-select-selection-ellipsis">⋯</span>
        <span className="multi-select-selection-count">{numItems}</span>
      </div>
    )
  );
};

export default MultiSelectionAddon;
