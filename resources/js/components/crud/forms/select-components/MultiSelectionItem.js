import React from "react";
import * as PropTypes from "prop-types";

const MultiSelectionItem = props => {
  let {
    id,
    label,
    onClick,
    innerRef,
    isVisible = true,
    ...nativeProps
  } = props;
  if (!isVisible) {
    return null;
  }
  return (
    <div
      className="multi-select-selection-item"
      ref={innerRef}
      {...nativeProps}
    >
      <span>{label}</span>
      <button onClick={() => onClick(id)}>{" x "}</button>
    </div>
  );
};

MultiSelectionItem.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  label: PropTypes.string,
  onClick: PropTypes.func,
  innerRef: PropTypes.object,
  isVisible: PropTypes.bool
};

export default MultiSelectionItem;
