import React, { Component } from "react";
import PropTypes from "prop-types";
import VirtualList from "react-tiny-virtual-list";
import Menu from "./Menu";
import Item from "./Item";
import MultiSelectionSearchInput from "./MultiSelectionSearchInput";
import SelectMenuContainer from "./SelectMenuContainer";

class MultiSelectionMenu extends Component {
  render() {
    const {
      downshift,
      itemToString,
      compareItems,
      isOpen,
      searchable,
      highlightedIndex,
      listItemHeight,
      maxListHeight,
      items,
      selection,
      inputRef
    } = this.props;
    const { getMenuProps, getItemProps } = downshift;
    const listItems = items.filter(item => {
      return !selection.find(sel => {
        return compareItems(sel, item);
      });
    });
    const listLength = listItems.length;
    return (
      <SelectMenuContainer isOpen={isOpen}>
        {searchable && (
          <MultiSelectionSearchInput
            key={`search-input-${downshift.id}`}
            downshift={downshift}
            innerRef={inputRef}
            isOpen={isOpen}
          />
        )}
        <Menu
          key={`items-menu-${downshift.id}`}
          isOpen={isOpen}
          getMenuProps={getMenuProps}
        >
          {selection.map((item, index) => {
            return (
              <Item
                key={`item-${downshift.id}-${item.id}`}
                getItemProps={getItemProps}
                itemAsString={itemToString(item)}
                listItem={item}
                active={true}
                index={index}
              />
            );
          })}
          {listItems.length > 0 && (
            <VirtualList
              width={"100%"}
              scrollToIndex={highlightedIndex || 0}
              scrollToAlignment="auto"
              height={
                listLength < 5 ? listLength * listItemHeight : maxListHeight
              }
              itemCount={listLength}
              itemSize={listItemHeight}
              renderItem={({ index, style }) => {
                const item = listItems[index];
                return (
                  <Item
                    key={`item-${downshift.id}-${item.id}`}
                    getItemProps={getItemProps}
                    itemAsString={itemToString(item)}
                    listItem={item}
                    active={false}
                    itemStyle={style}
                    index={index}
                  />
                );
              }}
            />
          )}
        </Menu>
      </SelectMenuContainer>
    );
  }
}

MultiSelectionMenu.defaultProps = {
  highlightedIndex: 0,
  isOpen: false,
  searchable: false,
  listItemHeight: 42,
  maxListHeight: 200,
  items: [],
  selection: [],
  compareItems: (a, b) => a === b
};

MultiSelectionMenu.propTypes = {
  downshift: PropTypes.object.isRequired,
  itemToString: PropTypes.func.isRequired,
  compareItems: PropTypes.func,
  highlightedIndex: PropTypes.number,
  isOpen: PropTypes.bool,
  searchable: PropTypes.bool,
  listItemHeight: PropTypes.number,
  items: PropTypes.array,
  maxListHeight: PropTypes.number,
  selection: PropTypes.array
};

export default MultiSelectionMenu;
