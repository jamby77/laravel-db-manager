import React from "react";
import cn from "classnames";

const SelectMenuContainer = ({ isOpen, children }) => {
  return (
    <div
      className={cn("base-select-menu", { open: isOpen })}
      style={{ display: isOpen ? "block" : "none" }}
    >
      {children}
    </div>
  );
};

export default SelectMenuContainer;
