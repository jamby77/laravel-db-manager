import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";
import { Octicon, Octicons } from "octicons-react";

const MultiSelectionSearchInput = ({
  downshift,
  innerRef,
  placeholder = "Search",
  isOpen = false
}) => {
  const { getInputProps, getLabelProps } = downshift;
  const style = { display: isOpen ? "flex" : "none" };
  return (
    <div
      className={cn("input-group", "multi-select-search-input")}
      style={style}
    >
      <label {...getLabelProps({ className: "sr-only" })}>{placeholder}</label>
      <input
        {...getInputProps({
          ref: innerRef,
          placeholder: placeholder,
          className: cn({ "base-select-input": true }, "form-control")
        })}
      />
      <div className="input-group-append">
        <div className="search-icon">
          <Octicon icon={Octicons.search} />
        </div>
      </div>
    </div>
  );
};

MultiSelectionSearchInput.propTypes = {
  downshift: PropTypes.object
};

export default MultiSelectionSearchInput;
