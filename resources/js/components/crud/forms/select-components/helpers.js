import matchSorter from "match-sorter";

export const itemToString = function(item) {
  if (typeof item === "string") {
    return item;
  }

  if (item === null) {
    return "";
  }
  if (!item.hasOwnProperty("text")) {
    throw "Select item must be a string or an object with 'text' field";
  }
  return item.text;
};

export const getItems = function(filter, allItems) {
  return filter
    ? matchSorter(allItems, filter, { keys: ["text", "id"] })
    : allItems;
};

export const compareItems = (a, b) => {
  if (a.id && b.id) {
    return a.id === b.id;
  }
  return a === b;
};

export const dummyHandleChange = item => {
  console.log(item);
};
