import React from "react";

const Menu = ({
  getMenuProps = () => {
    throw "getItemProps is required";
  },
  isOpen = false,
  children,
  ...rest
}) => {
  return (
    <ul {...rest} {...getMenuProps({ className: "base-select-menu-list" })}>
      {isOpen && children}
    </ul>
  );
};

export default Menu;
