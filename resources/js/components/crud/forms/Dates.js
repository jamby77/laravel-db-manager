import React from "react";
import Flatpickr from "react-flatpickr";
import cn from "classnames";
import { ErrorMessage } from "formik";
import { onDateChange } from "../../../misc/forms";

const Dates = props => {
  const {
    field: { name, value: fieldValue },
    form: { setFieldValue },
    label = name,
    containerClass,
    validation,
    ...rest
  } = props;
  let value = validation(fieldValue) ? fieldValue : undefined;
  return (
    <div className={cn("form-group", containerClass)}>
      <label htmlFor={name}>{label}</label>
      <Flatpickr
        {...rest}
        id={name}
        onChange={onDateChange(setFieldValue, name)}
        value={value}
      />
      <ErrorMessage name={name} />
    </div>
  );
};

export default Dates;
