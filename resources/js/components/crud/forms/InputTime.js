import React from "react";
import Dates from "./Dates";
import { dateTimeFormats, isValidTime } from "../../../misc/forms";
const InputTime = props => {
  return (
    <Dates
      {...props}
      options={{
        enableTime: true,
        allowInput: true,
        enableSeconds: true,
        noCalendar: true,
        dateFormat: dateTimeFormats.Time,
        time_24hr: true,
        static: true
      }}
      validation={isValidTime}
    />
  );
};

export default InputTime;
