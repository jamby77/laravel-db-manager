import React from "react";
import cn from "classnames";

const ControlPlaintext = ({ field, form, ...props }) => {
  const { name, value } = field;
  const { label = name, containerClass } = props;
  return (
    <div className={cn("form-group", containerClass)}>
      <label htmlFor={name}>{label}</label>
      <span className={`form-control-plaintext`}> {value}</span>
    </div>
  );
};

export default ControlPlaintext;
