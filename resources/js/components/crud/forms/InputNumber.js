import React from "react";
import cn from "classnames";
import { ErrorMessage } from "formik";

const InputNumber = ({ field, form, ...props }) => {
  const { name } = field;
  const { label = name, containerClass, ...rest } = props;
  return (
    <div className={cn("form-group", containerClass)}>
      <label htmlFor={name}>{label}</label>
      <input {...rest} {...field} id={name} type="number" />
      <ErrorMessage name={name} />
    </div>
  );
};

export default InputNumber;
