import React from "react";
import cn from "classnames";
import { ErrorMessage } from "formik";

const Textarea = ({ field, form, ...props }) => {
  const { name } = field;
  const { label = name, containerClass, ...rest } = props;
  return (
    <div className={cn("form-group", containerClass)}>
      <label htmlFor={name}>{label}</label>
      <textarea {...rest} {...field} id={name} />
      <ErrorMessage name={name} />
    </div>
  );
};

export default Textarea;
