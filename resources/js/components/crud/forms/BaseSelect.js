import React from "react";
import Downshift from "downshift";
import cn from "classnames";
import VirtualList from "react-tiny-virtual-list";
import { ToggleButton } from "./select-components/ToggleButton";
import Item from "./select-components/Item";
import Menu from "./select-components/Menu";
import {
  dummyHandleChange,
  getItems as defaultGetItems,
  itemToString as defaultItemToString
} from "./select-components/helpers";
import SelectMenuContainer from "./select-components/SelectMenuContainer";

const BaseSelect = ({
  value,
  items = [],
  placeholder = "Select",
  label = "Base List",
  listItemHeight = 42,
  maxListHeight = 200,
  handleChange = dummyHandleChange,
  itemToString = defaultItemToString,
  getItems = defaultGetItems
}) => {
  // handleChange = handleChange || dummyHandleChange
  return (
    <Downshift
      onChange={handleChange}
      itemToString={itemToString}
      initialSelectedItem={value}
    >
      {({
        getInputProps,
        getItemProps,
        getToggleButtonProps,
        getLabelProps,
        getMenuProps,
        isOpen,
        inputValue,
        highlightedIndex,
        selectedItem,
        clearSelection,
        id
      }) => {
        const listItems = getItems(inputValue, items);
        const listLength = listItems.length;
        return (
          <div className={cn({ "base-select": true })}>
            <label {...getLabelProps()}>{label}</label>
            <div className={cn("input-group")}>
              <input
                {...getInputProps({
                  placeholder: placeholder,
                  className: cn({ "base-select-input": true }, "form-control")
                })}
              />
              <div className={cn("input-group-append")}>
                {
                  <ToggleButton
                    selectedItem={selectedItem}
                    isOpen={isOpen}
                    buttonProps={getToggleButtonProps}
                    clearSelection={clearSelection}
                  />
                }
              </div>
            </div>
            <SelectMenuContainer isOpen={isOpen}>
              <Menu getMenuProps={getMenuProps} isOpen={isOpen}>
                <VirtualList
                  width={"100%"}
                  scrollToIndex={highlightedIndex || 0}
                  scrollToAlignment="auto"
                  height={
                    listLength < 5 ? listLength * listItemHeight : maxListHeight
                  }
                  itemCount={listLength}
                  itemSize={listItemHeight}
                  renderItem={({ index, style }) => (
                    <Item
                      key={`item-${id}-${listItems[index].id}`}
                      getItemProps={getItemProps}
                      active={selectedItem === listItems[index].id}
                      listItem={listItems[index]}
                      itemAsString={itemToString(listItems[index])}
                      index={index}
                      itemStyle={style}
                    />
                  )}
                />
              </Menu>
            </SelectMenuContainer>
          </div>
        );
      }}
    </Downshift>
  );
};

export default BaseSelect;
