import React from "react";
import { Field, Form, Formik } from "formik";
import InputText from "./InputText";
import ControlPlaintext from "./ControlPlaintext";
import InputNumber from "./InputNumber";
import InputCheckbox from "./InputCheckbox";
import Textarea from "./Textarea";
import InputDate from "./InputDate";
import InputDatetime from "./InputDatetime";
import InputTime from "./InputTime";
import InputSelect from "./InputSelect";
import { prepareSelectOptions } from "../../../misc/forms";

const fieldInput = field => {
  const { id, type } = field;
  if (type === "readonly") {
    return null;
  }

  const fieldProps = {
    name: id,
    className: `form-control`,
    containerClass: "",
    ...field
  };

  switch (type) {
    case "time":
      fieldProps.component = InputTime;
      fieldProps.placeholder = "Time";
      break;
    case "datetime":
      fieldProps.component = InputDatetime;
      fieldProps.placeholder = "Date Time";
      break;
    case "date":
      fieldProps.component = InputDate;
      fieldProps.placeholder = "Date";
      break;
    case "textarea":
      fieldProps.component = Textarea;
      fieldProps.placeholder = "Text";
      break;
    case "checkbox":
      fieldProps.component = InputCheckbox;
      break;
    case "number":
      fieldProps.component = InputNumber;
      fieldProps.placeholder = "Number";
      break;
    case "readonly":
      fieldProps.component = ControlPlaintext;
      break;
    case "multiple":
      fieldProps.component = InputSelect;
      fieldProps.options = prepareSelectOptions(field.options) || [];
      fieldProps.multiple = true;
      break;
    case "select":
      fieldProps.component = InputSelect;
      fieldProps.options = prepareSelectOptions(field.options) || [];
      break;
    default:
      fieldProps.component = InputText;
      fieldProps.placeholder = "String";
      break;
  }

  delete fieldProps.type;

  return <Field key={id} {...fieldProps} containerClass={"col-6"} />;
};

const EntityForm = ({
  entity,
  handleSubmit,
  formConfig,
  submitBinder,
  ...props
}) => {
  return (
    <div>
      <Formik
        initialValues={entity}
        onSubmit={handleSubmit}
        validateOnBlur={true}
      >
        {formProps => {
          const { status, submitForm, setErrors } = formProps;
          if (
            props.errors &&
            Object.keys(props.errors).length > 0 &&
            formProps.errors !== props.errors
          ) {
            setErrors(props.errors);
          }
          submitBinder(submitForm);
          return (
            <Form className={`entity-form row`}>
              {formConfig.map(f => {
                return fieldInput(f);
              })}
              {status && status.msg && <div>{status.msg}</div>}
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default EntityForm;
