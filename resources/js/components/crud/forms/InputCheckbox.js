import React from "react";
import { ErrorMessage } from "formik";
import FilterCheckbox from "../filters/FilterCheckbox";
const isOn = value => {
  const on =
    value !== 0 &&
    value !== "" &&
    value !== false &&
    value !== null &&
    value !== undefined;
  const off = !!value;
  return on;
};
const InputCheckbox = ({ field, form, ...props }) => {
  const { name, value } = field;
  const { label = name, containerClass, ...rest } = props;
  let handleChange = evt => {
    if (null === evt || undefined === evt || "" === evt) {
      form.setFieldValue(name, "");
      return;
    }
    form.setFieldValue(name, evt);
  };

  return (
    <div className={`form-group ${containerClass}`}>
      <div className={`form-check`}>
        <FilterCheckbox
          {...rest}
          {...field}
          className="form-check-input"
          id={name}
          value={value}
          onChange={handleChange}
        />
        <label className="form-check-label" htmlFor={name}>
          {label}
        </label>
      </div>
      <ErrorMessage name={name} />
    </div>
  );
};

export default InputCheckbox;
