import React from "react";
import Dates from "./Dates";
import { dateTimeFormats, isValidDateTime } from "../../../misc/forms";

const InputDate = props => {
  return (
    <Dates
      {...props}
      options={{
        enableTime: false,
        dateFormat: dateTimeFormats.Date,
        allowInput: true,
        static: true
      }}
      validation={isValidDateTime}
    />
  );
};

export default InputDate;
