import React from "react";
import Dates from "./Dates";
import { dateTimeFormats, isValidDateTime } from "../../../misc/forms";

const InputDatetime = props => {
  return (
    <Dates
      {...props}
      options={{
        enableTime: true,
        enableSeconds: true,
        dateFormat: dateTimeFormats.DateTime,
        time_24hr: true,
        allowInput: true,
        static: true
      }}
      validation={isValidDateTime}
    />
  );
};

export default InputDatetime;
