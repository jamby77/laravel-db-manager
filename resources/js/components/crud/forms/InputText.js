import React from "react";
import cn from "classnames";
import { ErrorMessage } from "formik";

const InputText = ({ field, form, ...props }) => {
  const { name, value = null } = field;
  const { label = name, containerClass, ...rest } = props;
  return (
    <div className={cn("form-group", containerClass)}>
      <label htmlFor={name}>{label}</label>
      <input {...rest} {...field} id={name} type="text" value={value} />
      <ErrorMessage name={name} className={"field-error"} component="div" />
    </div>
  );
};

export default InputText;
