import React, { Component } from "react";
import Downshift from "downshift";
import cn from "classnames";
import VirtualList from "react-tiny-virtual-list";
import { ToggleButton } from "./select-components/ToggleButton";
import Item from "./select-components/Item";
import Menu from "./select-components/Menu";

import {
  dummyHandleChange,
  getItems as defaultGetItems,
  itemToString as defaultItemToString,
  compareItems as defaultCompareItems
} from "./select-components/helpers";
import * as PropTypes from "prop-types";
import MultiSelectSelectionFormInput from "./select-components/MultiSelectSelectionFormInput";
import MultiSelectionMenu from "./select-components/MultiSelectionMenu";

class MultiSelect extends Component {
  constructor(props) {
    super(props);
    const selection = [...(props.values || [])];
    this.state = { selection };
    this.input = React.createRef();
  }

  stateReducer = (state, changes) => {
    switch (changes.type) {
      case Downshift.stateChangeTypes.keyDownEnter:
      case Downshift.stateChangeTypes.clickItem:
        let { inputValue } = changes;
        if (
          inputValue === changes.selectedItem &&
          inputValue !== state.inputValue
        ) {
          inputValue = state.inputValue;
        }
        return {
          ...changes,
          highlightedIndex: state.highlightedIndex,
          isOpen: true,
          inputValue
        };
      default:
        return changes;
    }
  };

  changeCallbacks = downshift => () => {
    const { handleSelect, handleChange } = this.props;
    const { selection } = this.state;
    if (handleSelect) {
      handleSelect(selection, this.getStateAndHelpers(downshift));
    }
    if (handleChange) {
      handleChange(selection, this.getStateAndHelpers(downshift));
    }
  };

  handleSelection = (selectedItem, downshift) => {
    if (this.state.selection.includes(selectedItem)) {
      this.removeItem(selectedItem, this.changeCallbacks(downshift));
    } else {
      this.addSelectedItem(selectedItem, this.changeCallbacks(downshift));
    }
  };

  removeItem = (item, cb) => {
    this.setState(({ selection }) => {
      return {
        selection: selection.filter(i => i !== item)
      };
    }, cb);
  };

  addSelectedItem = (item, cb) => {
    this.setState(
      ({ selection }) => ({
        selection: [...selection, item]
      }),
      cb
    );
  };

  clearSelection = () => {
    this.setState({ selection: [] }, this.changeCallbacks({}));
  };

  getRemoveButtonProps = ({ onClick, item, ...props } = {}) => {
    return {
      onClick: e => {
        // TODO: use something like downshift's composeEventHandlers utility instead
        onClick && onClick(e);
        e.stopPropagation();
        this.removeItem(item, this.changeCallbacks({}));
      },
      ...props
    };
  };

  getStateAndHelpers(downshift) {
    const { selectedItems } = this.state;
    const { getRemoveButtonProps, removeItem } = this;
    return {
      getRemoveButtonProps,
      removeItem,
      selectedItems,
      ...downshift
    };
  }

  render() {
    const {
      items,
      placeholder,
      label,
      getItems,
      itemToString,
      compareItems,
      ...rest
    } = this.props;
    const { selection } = this.state;
    return (
      <Downshift
        onChange={this.handleSelection}
        itemToString={itemToString}
        stateReducer={this.stateReducer}
        selectedItem={null}
      >
        {downshift => {
          const {
            getToggleButtonProps,
            getLabelProps,
            isOpen,
            inputValue,
            highlightedIndex,
            toggleMenu
          } = downshift;
          const listItems = getItems(inputValue, items);
          return (
            <div className={cn({ "base-select": true })}>
              <label {...getLabelProps()}>{label}</label>
              <MultiSelectSelectionFormInput
                {...rest}
                onSelectedItemClick={id =>
                  this.removeItem(id, this.changeCallbacks({}))
                }
                items={selection}
                clearSelection={this.clearSelection}
                getToggleButtonProps={getToggleButtonProps}
                isOpen={isOpen}
                onSelectionAddonClick={toggleMenu}
                placeholder={placeholder}
              />
              <MultiSelectionMenu
                {...rest}
                downshift={downshift}
                compareItems={compareItems}
                selection={selection}
                items={listItems}
                highlightedIndex={highlightedIndex}
                itemToString={itemToString}
                isOpen={isOpen}
                searchable={true}
              />
            </div>
          );
        }}
      </Downshift>
    );
  }
}

MultiSelect.propTypes = {
  values: PropTypes.arrayOf(PropTypes.any),
  items: PropTypes.arrayOf(PropTypes.any),
  placeholder: PropTypes.string,
  label: PropTypes.string,
  listItemHeight: PropTypes.number,
  maxListHeight: PropTypes.number,
  handleChange: PropTypes.func,
  getItems: PropTypes.func,
  itemToString: PropTypes.func,
  compareItems: PropTypes.func
};

MultiSelect.defaultProps = {
  values: [],
  items: [],
  placeholder: "Select",
  label: "Multi List",
  handleChange: dummyHandleChange,
  getItems: defaultGetItems,
  itemToString: defaultItemToString,
  compareItems: defaultCompareItems
};

export default MultiSelect;
