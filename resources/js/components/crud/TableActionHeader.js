import React, { Component, createRef } from "react";
import HeaderCell from "./HeaderCell";
import { CrudContext } from "../CrudContext";

class TableActionHeader extends Component {
  constructor(props) {
    super(props);
    this.ref = createRef();
    this.rect = null;
  }

  componentDidMount() {
    this.detectPosition(this.ref.current);
  }

  componentDidUpdate() {
    this.detectPosition(this.ref.current);
  }

  detectPosition(el) {
    if (!el) {
      return;
    }
    const rect = el.getBoundingClientRect();
    if (
      this.rect &&
      rect.width === this.rect.width &&
      rect.right === this.rect.right
    ) {
      return;
    }
    this.rect = rect;
    const { setActionsVisible } = this.context;

    if (setActionsVisible) {
      setActionsVisible(rect);
    }
  }

  render() {
    let { config } = this.props;
    return (
      <HeaderCell
        className={"sticky-top text-right"}
        style={config.style || {}}
        config={{ sortable: false }}
      >
        <div ref={this.ref}>Actions</div>
      </HeaderCell>
    );
  }
}
TableActionHeader.contextType = CrudContext;
export default TableActionHeader;
