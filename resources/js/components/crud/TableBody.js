import React from "react";
import { getRowKey } from "../../misc/helpers";
import Row from "./Row";

const TableBody = ({ data, columns, keyField, loading, long }) => {
  return (
    data && (
      <tbody>
        {(!loading && data.length === 0 && (
          <tr>
            <td colSpan={100}>
              <h1>Table has no content</h1>
            </td>
          </tr>
        )) ||
          (data.length > 0 &&
            data.map(row => (
              <Row
                keyField={keyField}
                key={getRowKey(row, keyField)}
                long={long}
                columns={columns}
                row={row}
              />
            )))}
      </tbody>
    )
  );
};

export default TableBody;
