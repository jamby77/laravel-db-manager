import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
const FilterNumber = ({
  onChange,
  enabled = true,
  className = "",
  value = "",
  placeholder = "",
  min,
  max,
  step
}) => {
  const props = {
    value: value,
    onChange: onChange,
    className: classnames([className, "input", "form-control"]),
    placeholder: placeholder,
    disabled: !enabled
  };
  if (min !== undefined && !isNaN(min)) {
    props.min = min;
  }
  if (max !== undefined && !isNaN(max)) {
    props.max = max;
  }
  if (step !== undefined && !isNaN(step)) {
    props.step = step;
  }
  return <input type="number" {...props} />;
};

FilterNumber.propTypes = {
  className: PropTypes.string,
  enabled: PropTypes.bool,
  max: PropTypes.number,
  min: PropTypes.number,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  value: PropTypes.any
};
export default FilterNumber;
