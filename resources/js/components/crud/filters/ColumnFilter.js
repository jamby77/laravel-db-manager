import React, { Component } from "react";
import PropTypes from "prop-types";
import cn from "classnames";
import * as FilterOptions from "./constants";
import Octicon, { Octicons } from "octicons-react/es/main";
import { Menu, MenuButton, MenuItem, MenuList } from "@reach/menu-button";
import "@reach/menu-button/styles.css";

class ColumnFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.defaultValue,
      operator: props.operator,
      enabled: props.enabled
    };
  }
  externalHandleChange = () => {
    if (this.props.onChange) {
      const { value, operator, enabled } = this.state;
      const { name } = this.props;
      this.props.onChange({
        name,
        value,
        operator,
        enabled
      });
    }
  };
  handleChange = e => {
    if (this.state.enabled === false) {
      return;
    }
    const { type } = this.props;
    if (type === FilterOptions.FilterTypes.CHECKBOX) {
      if (e === true) {
        this.setState({ value: true }, this.externalHandleChange);
      } else if (e === false) {
        this.setState({ value: false }, this.externalHandleChange);
      } else {
        this.clearFilter();
      }
      return;
    }
    this.setState({ value: e.target.value }, this.externalHandleChange);
  };

  setOperator = op => () => {
    if (this.state.enabled === false) {
      return;
    }
    this.setState({ operator: op }, this.externalHandleChange);
  };

  disableFilter = () => {
    this.setState({ enabled: false });
  };

  enableFilter = () => {
    this.setState({ enabled: true });
  };

  clearFilter = () => {
    this.setState({ value: null, checked: false }, this.externalHandleChange);
  };

  render() {
    const { type, placeholder, ...domProps } = this.props;
    let { value, enabled } = this.state;
    const FilterType = FilterOptions.FilterComponents[type];
    const filterOperators = FilterOptions[`${type}Operator`] || [];

    return (
      <div className={`db-column-filter input-group`}>
        <FilterType
          {...domProps}
          className="db-column-filter--filter"
          value={value}
          onChange={this.handleChange}
          placeholder={placeholder}
          enabled={enabled}
        />
        <div className="db-column-filter--menu input-group-append">
          <Menu>
            <MenuButton className={`btn bdn-default`}>
              <span aria-hidden>
                <Octicon icon={Octicons.gear} />
              </span>
              <MenuList className={"form-check"}>
                <MenuItem onSelect={this.clearFilter}>
                  <div
                    className={cn("db-column-filter-clear", {
                      disabled: !!!this.state.value
                    })}
                  >
                    Clear
                  </div>
                </MenuItem>
                <MenuItem onSelect={() => null} role={"none"}>
                  <hr />
                </MenuItem>
                {filterOperators.map(op => {
                  return (
                    <MenuItem key={op} onSelect={this.setOperator(op)}>
                      <div
                        className={cn("db-column-filter-operator", {
                          disabled: !this.state.enabled
                        })}
                      >
                        <input
                          readOnly
                          type="radio"
                          checked={op === this.state.operator}
                          className={"form-check-input"}
                        />
                        <label className={"form-check-label"}>{` ${op}`}</label>
                      </div>
                    </MenuItem>
                  );
                })}
                <MenuItem onSelect={() => null} role={"none"}>
                  <hr />
                </MenuItem>
                <MenuItem onSelect={this.enableFilter}>
                  <div
                    className={cn("db-column-filter-enable", {
                      disabled: this.state.enabled
                    })}
                  >
                    Enable
                  </div>
                </MenuItem>
                <MenuItem onSelect={this.disableFilter}>
                  <div
                    className={cn("db-column-filter-disable", {
                      disabled: !this.state.enabled
                    })}
                  >
                    Disable
                  </div>
                </MenuItem>
              </MenuList>
            </MenuButton>
          </Menu>
        </div>
      </div>
    );
  }
}

ColumnFilter.propTypes = {
  defaultValue: PropTypes.any,
  name: PropTypes.string,
  operator: PropTypes.oneOf(Object.values(FilterOptions.Operators)),
  placeholder: PropTypes.string,
  type: PropTypes.oneOf(Object.values(FilterOptions.FilterTypes)),
  onChange: PropTypes.func,
  enabled: PropTypes.bool
};

ColumnFilter.defaultProps = {
  defaultValue: null,
  name: "",
  type: FilterOptions.FilterTypes.TEXT,
  operator: FilterOptions.Operators.EQUAL,
  placeholder: "Search ...",
  onChange: function(a) {
    console.log(arguments);
  },
  enabled: true
};

export default ColumnFilter;
