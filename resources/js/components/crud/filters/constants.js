import FilterText from "./FilterText";
import FilterNumber from "./FilterNumber";
import FilterCheckbox from "./FilterCheckbox";
import FilterDate from "./FilterDate";
import FilterSelect from "./FilterSelect";

export const FilterTypes = {
  TEXT: "FilterText",
  NUMBER: "FilterNumber",
  CHECKBOX: "FilterCheckbox",
  DATETIME: "FilterDate",
  SELECT: "FilterSelect"
};

export const FilterComponents = {};
FilterComponents[FilterTypes.TEXT] = FilterText;
FilterComponents[FilterTypes.NUMBER] = FilterNumber;
FilterComponents[FilterTypes.CHECKBOX] = FilterCheckbox;
FilterComponents[FilterTypes.DATETIME] = FilterDate;
FilterComponents[FilterTypes.SELECT] = FilterSelect;

export const Operators = {
  CONTAINS: "contains",
  DOES_NOT_CONTAIN: "notContains",
  EMPTY: "empty",
  ENDS_WITH: "endsWith",
  EQUAL: "eq",
  GREATER_THAN: "gt",
  GREATER_THAN_OR_EQUAL: "gte",
  LESS_THAN: "lt",
  LESS_THAN_OR_EQUAL: "lte",
  NOT_EMPTY: "notEmpty",
  NOT_EQUAL: "neq",
  STARTS_WITH: "startsWith"
};

export const FilterTextOperator = [
  Operators.CONTAINS,
  Operators.DOES_NOT_CONTAIN,
  Operators.EQUAL,
  Operators.NOT_EQUAL,
  Operators.EMPTY,
  Operators.NOT_EMPTY,
  Operators.STARTS_WITH,
  Operators.ENDS_WITH
];

export const FilterCheckboxOperator = [
  Operators.EQUAL,
  Operators.EMPTY,
  Operators.NOT_EMPTY
];

export const FilterSelectOperator = [
  Operators.EQUAL,
  Operators.NOT_EQUAL,
  Operators.EMPTY,
  Operators.NOT_EMPTY
];

export const FilterNumberOperator = [
  Operators.LESS_THAN,
  Operators.LESS_THAN_OR_EQUAL,
  Operators.EQUAL,
  Operators.NOT_EQUAL,
  Operators.GREATER_THAN,
  Operators.GREATER_THAN_OR_EQUAL,
  Operators.EMPTY,
  Operators.NOT_EMPTY
];

export const FilterDateOperator = [
  Operators.LESS_THAN,
  Operators.LESS_THAN_OR_EQUAL,
  Operators.EQUAL,
  Operators.NOT_EQUAL,
  Operators.GREATER_THAN,
  Operators.GREATER_THAN_OR_EQUAL,
  Operators.EMPTY,
  Operators.NOT_EMPTY
];
