import React from "react";
import classnames from "classnames";
const FilterText = ({
  onChange,
  enabled = true,
  className = "",
  value = "",
  placeholder = ""
}) => {
  return (
    <input
      type="text"
      value={value}
      onChange={onChange}
      className={classnames([className, "input", "form-control"])}
      placeholder={placeholder}
      disabled={!enabled}
    />
  );
};

export default FilterText;
