import React from "react";
import classnames from "classnames";
import Octicon, { Octicons } from "octicons-react/es/main";

const handleClick = (enabled, checked, callback) => {
  if (!enabled) {
    return;
  }

  let checkboxValue = checked;

  if (checkboxValue === true) {
    checkboxValue = false;
  } else if (checkboxValue === false) {
    checkboxValue = null;
  } else {
    checkboxValue = true;
  }

  callback(checkboxValue);
};

const FilterCheckbox = ({
  onChange,
  enabled = true,
  className = "",
  value = null
}) => {
  const props = {
    className: classnames([className, "bool-filter"])
  };
  return (
    <div
      {...props}
      onClick={() => {
        handleClick(enabled, value, onChange);
      }}
      onKeyDown={e => {
        if (e.key === "Enter" || e.key === " ") {
          handleClick(enabled, value, onChange);
        }
      }}
    >
      <div
        role="checkbox"
        className={classnames({
          "bool-filter-on": enabled && value === true,
          "bool-filter-off": enabled && value === false,
          "bool-filter-neutral": enabled && (value === null || value === ""),
          "bool-filter-disabled": !enabled
        })}
      >
        <Octicon
          icon={
            value === false
              ? Octicons.dash
              : value === true
              ? Octicons.check
              : Octicons.primitiveSquare
          }
        />
      </div>
    </div>
  );
};

export default FilterCheckbox;
