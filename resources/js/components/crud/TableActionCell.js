import React, { Component, createRef } from "react";
import { Octicon, Octicons } from "octicons-react";
import classnames from "classnames";
import HeaderCell from "./HeaderCell";
import EditAction from "./tableActions/EditAction";
import * as PropTypes from "prop-types";
import CommentAction from "./tableActions/CommentAction";
import ViewAction from "./tableActions/ViewAction";
import DeleteAction from "./tableActions/DeleteAction";
import { CrudContext } from "../CrudContext";

const actionTypes = {
  View: "view",
  Edit: "edit",
  Delete: "delete",
  Comment: "comment"
};
const btnClassNames = btn => {
  const defaultClassNames = { btn: true, "btn-default": true, "btn-sm": true };
  switch (btn) {
    case "view":
      defaultClassNames["btn-default"] = false;
      defaultClassNames["btn-primary"] = true;
      break;
    case "edit":
      defaultClassNames["btn-default"] = false;
      defaultClassNames["btn-warning"] = true;
      break;
    case "delete":
      defaultClassNames["btn-default"] = false;
      defaultClassNames["btn-danger"] = true;
      break;
    case "comment":
    default:
      defaultClassNames["btn-default"] = false;
      defaultClassNames["btn-light"] = true;
      break;
  }
  return classnames(defaultClassNames);
};
const btnIcon = btn => {
  let icon;
  switch (btn) {
    case "view":
      icon = Octicons.search;
      break;
    case "edit":
      icon = Octicons.pencil;
      break;
    case "delete":
      icon = Octicons.trashcan;
      break;
    case "comment":
    default:
      icon = Octicons.comment;
      break;
  }
  return <Octicon icon={icon} />;
};
const initialState = {
  inAction: false,
  action: null,
  hasError: false,
  errors: [],
  errorMessage: "",
  loading: false
};

class TableActionCell extends Component {
  closeBtnRef;
  constructor(props) {
    super(props);
    this.state = { ...initialState };
    this.defaultClickHandler = this.defaultClickHandler.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.closeActionDialog = this.closeActionDialog.bind(this);
    this.closeBtnRef = createRef();
    this.actionsRef = createRef();
  }

  componentDidMount() {
    this.getCurrentDims();
  }

  getCurrentDims() {
    if (!this.actionsRef.current) {
      return;
    }
    const rect = this.actionsRef.current.getBoundingClientRect();
    this.setState({ initialRect: rect });
  }

  defaultClickHandler(e, btn) {
    this.setState({ action: btn.type, inAction: true });
  }

  loadRowHandler(btn, id) {
    const { fetchRow, currentTable: table } = this.context;
    if (fetchRow) {
      fetchRow(table.name, id).then(response => {
        this.setState({ action: btn.type, inAction: true, row: response.data });
      });
    }
  }

  handleClick(btn) {
    return e => {
      if (btn.onClick) {
        return btn.onClick(e, btn);
      }

      if (btn.type === actionTypes.Edit || btn.type === actionTypes.View) {
        return this.loadRowHandler(btn, btn.id);
      }
      return this.defaultClickHandler(e, btn);
    };
  }
  closeActionDialog() {
    this.setState({ ...initialState });
  }
  render() {
    let { config, hovered, topOffset, ...rest } = this.props;
    if (!config) {
      return null;
    }
    const style = {};
    if (hovered) {
      style.right = "0.75rem";
      style.top = `${this.state.initialRect.top - topOffset}px`;
    }
    return (
      <HeaderCell
        {...rest}
        className={classnames("row-actions", "text-right")}
        config={{ sortable: false }}
      >
        <div
          className="btn-group action-buttons"
          style={style}
          ref={this.actionsRef}
        >
          {Object.keys(config)
            .filter(btn => !!config[btn])
            .map(btn => {
              const b = config[btn];
              b.type = btn;
              return (
                <button
                  className={btnClassNames(btn)}
                  title={b.title}
                  onClick={this.handleClick(b)}
                  key={`${btn}-${b.id}`}
                >
                  {btnIcon(btn)}
                </button>
              );
            })}
        </div>
        {this.state.inAction && this.renderActionDialog()}
      </HeaderCell>
    );
  }

  renderActionDialog() {
    const {
      row,
      action: type,
      loading,
      hasError,
      errors,
      errorMessage
    } = this.state;
    const action = this.props.config[type];
    if (!action) {
      return null;
    }
    const {
      currentTable,
      deleteRecord,
      updateRecord,
      loadTable
    } = this.context;
    const actionProps = {
      id: action.id,
      onDismiss: this.closeActionDialog,
      onConfirm: e => console.log(e),
      focusRef: this.closeBtnRef,
      row,
      loading,
      hasError,
      errors,
      errorMessage
    };

    switch (type) {
      case actionTypes.Edit:
        actionProps.onConfirm = values => {
          updateRecord(currentTable.name, action.id, values)
            .then(response => {
              if (response.status === 200) {
                this.closeActionDialog();
                loadTable(currentTable.name);
              }
            })
            .catch(({ response }) => {
              // debugger;
              const errors = {};
              if (response.data.errors) {
                for (let field in response.data.errors) {
                  errors[field] = response.data.errors[field].join("\n");
                }
              }
              this.setState({
                hasError: true,
                errors,
                errorMessage: response.data.message
              });
            });
        };
        return <EditAction {...actionProps} />;
      case actionTypes.View:
        return <ViewAction {...actionProps} />;
      case actionTypes.Delete:
        actionProps.onConfirm = () => {
          deleteRecord(currentTable.name, action.id)
            .then(response => {
              if (response.status === 200) {
                this.closeActionDialog();
                loadTable(currentTable.name);
              }
            })
            .catch(error => {
              console.log(error);
            });
        };
        return <DeleteAction {...actionProps} />;
      case actionTypes.Comment:
        return <CommentAction {...actionProps} />;
    }
    return null;
  }
}
TableActionCell.contextType = CrudContext;
TableActionCell.propTypes = {
  config: PropTypes.object
};

export default TableActionCell;
