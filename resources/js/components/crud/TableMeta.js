import React from "react";
import cn from "classnames";
import { fileSize, formatNumber } from "../../misc/helpers";

const defaultTitle = "Table Information:";
const fieldsOfInterest = [
  "database",
  "created",
  "updated",
  "engine",
  "rows",
  "size",
  "encoding",
  "auto_increment"
];
const fieldsOfInterestMap = {
  database: "table_schema",
  created: "create_time",
  updated: "update_time",
  engine: "engine",
  rows: "table_rows",
  size: "data_length",
  encoding: "table_collation",
  auto_increment: "auto_increment"
};

const generateMetaEntries = meta => f => {
  const dbField = fieldsOfInterestMap[f];
  let data = meta[dbField];
  if (!data) {
    return null;
  }
  if (f === "size") {
    data = fileSize(data);
  }
  if (f === "rows" || f === "auto_increment") {
    data = formatNumber(data);
  }
  if (f === "encoding") {
    data = data.split("_")[0];
  }
  return (
    <div
      key={f}
      className={cn(
        "d-flex",
        "flex-row",
        "justify-content-between",
        "meta-field"
      )}
    >
      <dt>{f}</dt>
      <dd>{data}</dd>
    </div>
  );
};

const TableMeta = ({ title = defaultTitle, wrapperClass = {}, meta }) => {
  if (!meta) {
    return null;
  }
  const wrapperClasses = cn(wrapperClass, "meta-info");
  const metaEntriesCallback = generateMetaEntries(meta);
  return (
    <div className="meta">
      <h5>{title}</h5>
      <div className={wrapperClasses}>
        {fieldsOfInterest.map(metaEntriesCallback)}
      </div>
    </div>
  );
};

export default TableMeta;
