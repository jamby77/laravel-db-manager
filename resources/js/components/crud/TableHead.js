import React from "react";
import cn from "classnames";
import TableActionHeader from "./TableActionHeader";
import HeaderCell from "./HeaderCell";
import { actionsCol } from "../../misc/helpers";
import { Consumer } from "../CrudContext";
import { withRouter } from "react-router-dom";
import Thead from "./table/Thead";
import TableRow from "./table/TableRow";
import FiltersRow from "./table/FiltersRow";

const tableHeadClasses = ["thead-dark"];

const TableHead = ({ columns, match }) => {
  return (
    <Consumer>
      {context => {
        const { params, setSortOn } = context;
        const currentUrlParams = params(context.currentTable.name, context);
        return (
          <Thead className={cn(tableHeadClasses)}>
            <TableRow className={`dbmanager-grid-header`}>
              {columns.map(col =>
                col.field === actionsCol ? (
                  <TableActionHeader key={"action-header"} config={col} />
                ) : (
                  <HeaderCell
                    config={col}
                    urlParams={currentUrlParams}
                    onSort={setSortOn}
                    className={cn("sticky-top")}
                    style={col.style}
                    key={col.field}
                    url={match.url}
                  >
                    {col.label}
                  </HeaderCell>
                )
              )}
            </TableRow>
            {context.enableFilters && <FiltersRow columns={columns} />}
          </Thead>
        );
      }}
    </Consumer>
  );
};

export default withRouter(TableHead);
