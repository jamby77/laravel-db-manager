import { createContext } from "react";
import { AppStates } from "./AppState";

export const crudValue = {
  tablesUpdatedAt: null,
  state: AppStates.Initial,
  actionsLong: false,
  rightPos: 0,
  table: null,
  filters: {},
  sort_on: {},
  hasError: false,
  error: null,
  pagerHeight: 100,
  minSideBarWidth: 100,
  showSidebar: true,
  pagination: {
    pageSizeOptions: [10, 20, 50, 100, -1],
    numPages: 10,
    onEachSide: 2,
    perPage: 10,
    page: 1
  },
  database: {
    tables: [],
    name: null
  },
  currentTable: {
    name: "",
    data: [],
    meta: {}
  },
  params(table, state) {
    const { perPage, page = 1 } = state.pagination;
    const { filters = {}, sort_on = {} } = state;
    return {
      pagesize: perPage,
      page,
      ...filters[table],
      ...sort_on[table]
    };
  }
};
export const CrudContext = createContext(crudValue);

export const { Consumer, Provider } = CrudContext;
