export const AppStates = {
  Initial: 0,
  Loading: 1,
  Loaded: 2,
  Error: 3
};
