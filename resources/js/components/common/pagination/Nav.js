import React from "react";

const Nav = ({ table, children }) => {
  return (
    <nav
      className="nav flex-row justify-content-between"
      aria-label={`Pagination options for ${table} table`}
    >
      {children}
    </nav>
  );
};

export default Nav;
