import React from "react";
import PageLink from "./PageLink";
import { getNextPageNum, getPrevPageNum } from "../../../misc/helpers";
import GotoPage from "./GotoPage";

const Pages = ({
  pages,
  url,
  currentPage,
  lastPage,
  setPagination,
  currentUrlParams,
  showGotoPage
}) => {
  const prevPage = getPrevPageNum(currentPage);
  const nextPage = getNextPageNum(currentPage, lastPage);
  return (
    <ul className="pagination p-md-3 p-sm-0" role="navigation">
      <PageLink
        key={"previous-page"}
        page={prevPage}
        currentPage={currentPage}
        url={url}
        setPage={setPagination}
        params={{ ...currentUrlParams, ...{ page: prevPage } }}
        label={"Previous"}
      />
      {pages.map((page, i) => {
        return (
          <PageLink
            key={`pagenum-${page}-${i}`}
            page={page}
            currentPage={currentPage}
            url={url}
            setPage={setPagination}
            params={{ ...currentUrlParams, ...{ page } }}
          />
        );
      })}
      <PageLink
        key={"next-page"}
        page={nextPage}
        currentPage={currentPage}
        url={url}
        setPage={setPagination}
        params={{ ...currentUrlParams, ...{ page: nextPage } }}
        label={"Next"}
      />
      {showGotoPage && (
        <li className="page-item">
          <GotoPage onPageChange={setPagination} lastPage={lastPage} />
        </li>
      )}
    </ul>
  );
};

export default Pages;
