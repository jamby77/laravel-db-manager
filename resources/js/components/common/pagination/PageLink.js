import React from "react";
import { delim } from "../../../misc/helpers";
import { Link } from "react-router-dom";
import { stringify } from "query-string";
const labeledLinks = [
  "first",
  "last",
  "previous",
  "next",
  ">",
  ">>",
  "<",
  "<<"
];
const PageLink = ({ page, currentPage, url, setPage, params, label = "" }) => {
  if (page === delim) {
    return (
      <li className="page-item disabled" aria-disabled="true">
        <span className="page-link">&hellip;</span>
      </li>
    );
  }
  if (
    label &&
    page === currentPage &&
    labeledLinks.includes(label.toLowerCase())
  ) {
    return (
      <li className="page-item disabled" aria-disabled="true">
        <span className="page-link">{label}</span>
      </li>
    );
  }

  if (page === currentPage) {
    return (
      <li className="page-item active" aria-current="page">
        <span className="page-link">{label || page}</span>
      </li>
    );
  }
  return (
    <li className="page-item">
      <Link
        onClick={() => setPage({ page })}
        className="page-link"
        to={{
          pathname: url,
          search: stringify(params)
        }}
      >
        {label || page}
      </Link>
    </li>
  );
};

export default PageLink;
