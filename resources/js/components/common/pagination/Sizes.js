import React from "react";
import { showPageSizeOption } from "../../../misc/helpers";
import PageLink from "./PageLink";

const Sizes = ({
  from,
  to,
  total,
  url,
  pageSizeOptions,
  currentPageSize,
  setPagination,
  currentUrlParams
}) => {
  return (
    <ul className="limit-buttons pagination p-md-3 p-sm-0">
      <li className="page-item disabled">
        <span className="page-link border-0">
          Showing <strong>{from}</strong> to <strong>{to}</strong> from{" "}
          <strong>{total}</strong>
        </span>
      </li>
      {pageSizeOptions.map(
        size =>
          showPageSizeOption(size, total, pageSizeOptions) && (
            <PageLink
              key={`pagesize-${size}`}
              page={size}
              currentPage={currentPageSize}
              url={url}
              setPage={() => setPagination({ perPage: size })}
              params={{ ...currentUrlParams, ...{ pagesize: size } }}
              label={size === -1 ? "All" : size.toString()}
            />
          )
      )}
    </ul>
  );
};

export default Sizes;
