import React, { Component } from "react";
import PropTypes from "prop-types";

class GotoPage extends Component {
  constructor(props) {
    super(props);
    this.input = React.createRef();
  }

  handleGotoPage = () => {
    const page = parseInt(this.input.current.value, 10);
    if (isNaN(page)) {
      return;
    }
    const { firstPage, lastPage, onPageChange } = this.props;
    if (page >= firstPage && page <= lastPage) {
      onPageChange({ page });
      this.input.current.value = "";
    }
  };

  handleEnter = evt => {
    if (evt.key === "Enter") {
      this.handleGotoPage();
      evt.stopPropagation();
    }
  };

  render() {
    let { lastPage, firstPage, placeholder, goLabel } = this.props;
    return (
      <div className="goto-page">
        <div className="input-group">
          <div className="input-group-prepend">
            <input
              ref={this.input}
              type="number"
              className="form-control"
              step={1}
              min={firstPage}
              max={lastPage}
              placeholder={placeholder}
              onKeyUp={this.handleEnter}
            />
          </div>
          <div className="input-group-append">
            <button className="btn btn-primary" onClick={this.handleGotoPage}>
              {goLabel}
            </button>
          </div>
        </div>
      </div>
    );
  }
}

GotoPage.defaultProps = {
  firstPage: 1,
  placeholder: "Go to page",
  goLabel: "Go"
};

GotoPage.propTypes = {
  firstPage: PropTypes.number,
  goLabel: PropTypes.string,
  lastPage: PropTypes.number,
  onPageChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string
};
export default GotoPage;
