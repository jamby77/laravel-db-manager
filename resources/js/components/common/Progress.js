import React from "react";
import classnames from "classnames";

const Progress = ({ className, complete = 100, children }) => {
  const classes = classnames({
    ...(className || {}),
    ...{
      "progress-bar": true,
      "bg-dark": true,
      "progress-bar-striped": true,
      "progress-bar-animated": true
    }
  });
  return (
    <div className="progress" style={{ height: "20px" }}>
      <div
        className={classes}
        role="progressbar"
        aria-valuenow={`${complete}`}
        aria-valuemin="0"
        aria-valuemax="100"
        style={{
          width: `${complete}%`
        }}
      >
        {children}
      </div>
    </div>
  );
};

export default Progress;
