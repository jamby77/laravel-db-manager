import React from "react";

function alertClass(type) {
  let className = "alert-";
  switch (type) {
    case "success":
      className += "success";
      break;
    case "error":
      className += "danger";
      break;
    case "info":
      className += "info";
      break;
    case "warning":
      className += "warning";
      break;
    default:
      className += "primary";
      break;
  }
  return className;
}

const AlertMessage = ({ type, message }) => {
  return (
    <div className={`alert ${alertClass(type)}`} role="alert">
      {message}
    </div>
  );
};

export default AlertMessage;
