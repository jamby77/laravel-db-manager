import React from "react";
import { withRouter } from "react-router-dom";
import { Consumer } from "../CrudContext";
import { delim, getPages } from "../../misc/helpers";
import Sizes from "./pagination/Sizes";
import Pages from "./pagination/Pages";
import Nav from "./pagination/Nav";

export const Pagination = ({
  total,
  table,
  from,
  to,
  url,
  pageSizeOptions,
  setPagination,
  currentPageSize,
  currentUrlParams,
  pages,
  currentPage,
  lastPage
}) => {
  return (
    total > 0 && (
      <Nav table={table}>
        <Sizes
          from={from}
          to={to}
          total={total}
          pageSizeOptions={pageSizeOptions}
          url={url}
          setPagination={setPagination}
          currentPageSize={currentPageSize}
          currentUrlParams={currentUrlParams}
        />
        {pages.length > 0 && (
          <Pages
            currentPage={currentPage}
            lastPage={lastPage}
            pages={pages}
            url={url}
            setPagination={setPagination}
            currentUrlParams={currentUrlParams}
            showGotoPage={pages.findIndex(p => p === delim)}
          />
        )}
      </Nav>
    )
  );
};

const PaginationController = ({ match }) => {
  return (
    <Consumer>
      {context => {
        const { pageSizeOptions, onEachSide } = context.pagination;
        if (!context.currentTable.pagination) {
          return null;
        }
        const { setPagination, params } = context;
        const currentUrlParams = params(context.currentTable.name, context);
        const {
          current_page,
          last_page,
          per_page,
          total,
          from,
          to
        } = context.currentTable.pagination;
        const currentPageSize = parseInt(per_page, 10);
        const pages = getPages({
          lastPage: last_page,
          onEachSide,
          currentPage: current_page
        });

        return (
          <Pagination
            {...{
              total,
              from,
              to,
              pageSizeOptions,
              currentPageSize,
              pages,
              setPagination,
              currentUrlParams,
              table: context.currentTable.name,
              url: match.url,
              currentPage: current_page,
              lastPage: last_page
            }}
          />
        );
      }}
    </Consumer>
  );
};

export default withRouter(PaginationController);
