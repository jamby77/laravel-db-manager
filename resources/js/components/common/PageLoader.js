import React from "react";
import {
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogLabel,
  AlertDialog
} from "@reach/alert-dialog";
import VisuallyHidden from "@reach/visually-hidden";
import { Octicon, Octicons } from "octicons-react/es/main";

const PageLoader = props => {
  const { cancelRef } = props;
  return (
    <div>
      <AlertDialog
        leastDestructiveRef={cancelRef}
        style={{ backgroundColor: "transparent" }}
      >
        <AlertDialogContent className={"page-loader"}>
          <AlertDialogLabel>
            <VisuallyHidden>Loading...</VisuallyHidden>
          </AlertDialogLabel>
          <AlertDialogDescription>
            <div
              ref={cancelRef}
              className={"text-dark"}
              style={{
                background: "transparent",
                border: "0 none"
              }}
            >
              <Octicon scale={5} icon={Octicons.sync} className={"loading"} />
            </div>
          </AlertDialogDescription>
        </AlertDialogContent>
      </AlertDialog>
    </div>
  );
};

export default PageLoader;
