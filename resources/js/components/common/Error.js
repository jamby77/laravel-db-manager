import React, { Component } from "react";

class Error extends Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    return { hasError: true, error };
  }

  componentDidCatch(error, info) {
    console.log(error, info);
  }

  render() {
    if (this.state.hasError) {
      return (
        <div className={`container`}>
          <h1>Something went wrong</h1>
          <pre className={"bg-danger text-white"}>{this.state.error.stack}</pre>
        </div>
      );
    }

    return this.props.children;
  }
}

export default Error;
