import React from "react";
import cn from "classnames";
import { Octicon, Octicons } from "octicons-react/es/main";

const sidebarClasses = ["bg-dark", "text-light", "sticky-top", "p-0"];

const SidebarCollapsed = ({ toggleSidebar }) => {
  return (
    <aside className={cn(sidebarClasses)}>
      <div className="sidebar">
        <header>
          <div>
            <button
              onClick={toggleSidebar}
              className="btn btn-default btn-collapse"
              title="Expand Sidebar"
            >
              <Octicon icon={Octicons.threeBars} />
            </button>
          </div>
        </header>
      </div>
    </aside>
  );
};

export default SidebarCollapsed;
