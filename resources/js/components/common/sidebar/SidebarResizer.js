import React, { Component } from "react";
import { CrudContext } from "../../CrudContext";

class SidebarResizer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resizing: false,
      newLeft: 0
    };
    this.handleResize = this.handleResize.bind(this);
    this.stopResize = this.stopResize.bind(this);
    this.startResize = this.startResize.bind(this);
  }

  handleResize(e) {
    if (this.state.resizing && e.pageX >= this.context.minSideBarWidth) {
      this.setState({ newLeft: e.pageX });
    }
  }

  stopResize() {
    this.setState({ resizing: false }, () => {
      if (this.context && this.context.resize) {
        this.context.resize(this.state.newLeft);
      }
    });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.state.resizing) {
      window.addEventListener("mousemove", this.handleResize);
      window.addEventListener("mouseup", this.stopResize);
    } else {
      window.removeEventListener("mousemove", this.handleResize);
    }
  }

  componentWillUnmount() {
    window.removeEventListener("mouseup", this.stopResize);
    window.removeEventListener("mousemove", this.handleResize);
  }

  startResize() {
    return this.setState({ resizing: true });
  }

  render() {
    const left = this.state.newLeft || this.context.sideBarWidth;
    const style = {
      position: "absolute",
      width: "4px",
      top: "0px",
      right: "auto",
      bottom: "0px",
      height: "auto",
      left: `${left}px`
    };

    if (this.state.resizing) {
      style.border = "1px inset #fff";
    }

    return (
      <div style={style} className="resizer" onMouseDown={this.startResize} />
    );
  }
}
SidebarResizer.contextType = CrudContext;

export default SidebarResizer;
