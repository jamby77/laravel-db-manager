import React from "react";
import cn from "classnames";
import { Octicon, Octicons } from "octicons-react/es/main";

const refreshButtonClasses = ["btn", "btn-dark", "w-100", "text-uppercase"];

const RefreshTables = ({ refreshTables }) => {
  return (
    <button className={cn(refreshButtonClasses)} onClick={refreshTables}>
      <span className="d-flex flex-row justify-content-around align-items-center">
        <span>Refresh Tables</span>
        <Octicon icon={Octicons.sync} scale={1.5} />
      </span>
    </button>
  );
};

export default RefreshTables;
