import React from "react";
import cn from "classnames";
import { Octicon, Octicons } from "octicons-react/es/main";
import { NavLink } from "react-router-dom";
import RefreshTables from "./RefreshTables";
import SidebarResizer from "./SidebarResizer";

const sidebarClasses = ["bg-dark", "text-light", "sticky-top", "p-0"];

const SidebarFull = ({
  tables,
  loadTable,
  refreshTables,
  style,
  tablesUpdatedAt,
  loading,
  toggleSidebar
}) => {
  return (
    <aside className={cn(sidebarClasses)} style={style}>
      <div className={cn("sidebar")}>
        <header>
          <div>
            <button
              onClick={toggleSidebar}
              className="btn btn-default btn-collapse"
            >
              <Octicon icon={Octicons.threeBars} /> Hide Sidebar
            </button>
          </div>
          <div className={cn("small")}>
            {tablesUpdatedAt ? (
              <span>
                Last refreshed <em>{tablesUpdatedAt}</em>
              </span>
            ) : (
              "Refreshing"
            )}
          </div>
        </header>
        <ul className={cn(["nav", "flex-column", "my-2"])}>
          {tables.map(t => {
            const tableLinkClasses = ["nav-link", "text-white"];
            return (
              <li key={t.name} className={cn(["nav-item"])}>
                <NavLink
                  onClick={() => loadTable(t.name)}
                  activeClassName={"active"}
                  to={`/${window.laraPMA.basePath}/${t.name}`}
                  className={cn(tableLinkClasses)}
                >
                  {t.name}
                </NavLink>
              </li>
            );
          })}
        </ul>
        <footer style={{ visibility: loading ? "hidden" : "visible" }}>
          <RefreshTables refreshTables={refreshTables} />
        </footer>
      </div>
      <SidebarResizer />
    </aside>
  );
};

export default SidebarFull;
