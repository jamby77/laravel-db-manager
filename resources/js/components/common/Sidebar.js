import React from "react";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import { Consumer } from "../CrudContext";
import Error from "./Error";
import { AppStates } from "../AppState";
import SidebarFull from "./sidebar/SidebarFull";
import SidebarCollapsed from "./sidebar/SidebarCollapsed";

dayjs.extend(relativeTime);

const Sidebar = props => (
  <Error>
    <Consumer>
      {context => {
        const {
          database: { tables },
          loadTable,
          refreshTables,
          tablesUpdatedAt,
          maxHeight,
          sideBarWidth,
          state,
          showSidebar,
          toggleSidebar
        } = context;
        if (!showSidebar) {
          return <SidebarCollapsed toggleSidebar={toggleSidebar} />;
        }
        const loading =
          state === AppStates.Loading || state === AppStates.Initial;
        const styles = maxHeight
          ? {
              height: `${maxHeight}px`,
              width: `${sideBarWidth}px`,
              position: "absolute",
              bottom: "0px",
              top: "0px",
              left: "0px",
              right: "auto"
            }
          : {};
        return (
          <SidebarFull
            tables={tables}
            style={styles}
            loadTable={loadTable}
            refreshTables={refreshTables}
            tablesUpdatedAt={tablesUpdatedAt && tablesUpdatedAt.fromNow()}
            loading={loading}
            toggleSidebar={toggleSidebar}
          />
        );
      }}
    </Consumer>
  </Error>
);
export default Sidebar;
