import cn from "classnames";
import React from "react";

const headerClasses = ["row", "sticky-top", "shadow-sm"];
const Header = () => (
  <header className={cn(headerClasses)}>
    <h1 className={cn(["display-1"])}>
      <a href="/">Lara PMA</a>
    </h1>
  </header>
);

export default Header;
