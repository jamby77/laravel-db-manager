import { actionsCol, isScalar } from "./helpers";
import isNumber from "lodash/isNumber";
import isObject from "lodash/isObject";
import * as yup from "yup";

import dayjs from "dayjs";
import { FilterTypes } from "../components/crud/filters/constants";

export const dateTimeFormats = {
  Date: "Y-m-d",
  Time: "H:i:S",
  DateTime: "Y-m-d H:i:S"
};

export const isValidDateTime = dateValue => {
  const date = dayjs(dateValue);
  return (
    dateValue &&
    dateValue !== "0000-00-00" &&
    dateValue !== "0000-00-00 00:00:00" &&
    date.isValid()
  );
};

export const parseFlatTime = (time, lvl = 0) => {
  if (!isNumber(time) || isNaN(time)) {
    return 0;
  }
  // this is supposed to parse time like 1234 into 12 min 34 sec
  const neg = time < 0 ? -1 : 1;
  time = Math.abs(time);
  if (time <= 59) {
    return time * Math.pow(60, lvl) * neg;
  }
  if ((time > 59 && time < 100 && lvl < 2) || (lvl >= 2 && time >= 839)) {
    return 0;
  } else if (lvl >= 2) {
    return time * 3600 * neg;
  }
  return (
    (parseFlatTime(Math.floor(time / 100), lvl + 1) +
      parseFlatTime(time % 100, lvl)) *
    neg
  );
};

export const getTimeSeconds = value => {
  if (!value || isObject(value)) {
    return 0;
  }
  if (isNumber(value)) {
    // if number is passed, it is taken as the value in seconds
    return value;
  }

  if (value.length > 7) {
    const date = dayjs(value);
    if (date.isValid()) {
      const startOf = date.startOf("day");
      // return only hour:min part
      return date.diff(startOf, "second");
    }
  }

  if (value.indexOf(":") > 0) {
    // split mysql time string 00:14:23 - 14 min and 23 sec
    const timeParts = value.split(":").map(p => parseInt(p, 10));
    let invalid = false;
    timeParts.forEach(p => {
      if (!isNumber(p) || isNaN(p) || Infinity === p) invalid = true;
    });
    if (invalid) {
      return 0;
    }
    // if less than 3 part time is given, assume it is hrs:min format or just hrs
    while (timeParts.length < 3) {
      timeParts.push(0);
    }
    return timeParts.reduce((total, part, idx) => {
      const secs = Math.pow(60, 2 - idx);
      const neg = total < 0 || (part < 0 && idx === 0);
      total = Math.abs(total) + Math.abs(part) * secs;
      return neg ? -total : total;
    }, 0);
  }

  let time = parseInt(value, 10);
  return parseFlatTime(time);
};

export const isValidTime = timeValue => {
  const timeInsecs = getTimeSeconds(timeValue);
  return timeInsecs !== 0;
};

export const filterType = col => {
  let type = FilterTypes.TEXT;

  switch (col["type"]) {
    case "bigint":
    case "integer":
    case "smallint":
    case "float":
    case "decimal":
      type = FilterTypes.NUMBER;
      break;
    case "time":
    case "date":
    case "datetime":
      type = FilterTypes.DATETIME;
      break;
    case "boolean":
      type = FilterTypes.CHECKBOX;
      break;
    case "enum":
      type = FilterTypes.SELECT;
      break;
    case "set":
      type = FilterTypes.SELECT;
      break;
    default:
      break;
  }

  if (Array.isArray(col.options)) {
    type = FilterTypes.SELECT;
  }
  return type;
};
export const supportedInputTypes = [
  "text",
  "number",
  "time",
  "date",
  "datetime",
  "checkbox",
  "textarea",
  "string",
  "select",
  "multiple"
];

export const inputType = col => {
  let type = "text";
  if (col["autoincrement"]) {
    // skip autoincrement
    return "readonly";
  }
  switch (col["type"]) {
    case "bigint":
    case "integer":
    case "smallint":
    case "float":
    case "decimal":
      type = "number";
      break;
    case "time":
      type = "time";
      break;
    case "date":
      type = "date";
      break;
    case "datetime":
      type = "datetime";
      break;
    case "boolean":
      type = "checkbox";
      break;
    case "blob":
    case "text":
      type = "textarea";
      break;
    case "string":
    case "simple_array":
      type = "string";
      break;
    case "enum":
      type = "select";
      break;
    case "set":
      type = "multiple";
      break;
    default:
      break;
  }

  return type;
};

const maxP = m => {
  let n = 9;
  while (n.toString().length <= m) {
    n += n * 10;
  }
  return n;
};

export const prepareValidationSchema = columns => {
  const validations = {};
  columns.forEach(c => {
    if (c["autoincrement"] || c["field"] === actionsCol) {
      // skip autoincrement
      return;
    }
    let schema;
    switch (c["type"]) {
      case "bigint":
      case "integer":
      case "smallint":
      case "float":
      case "decimal":
        schema = yup.number();
        if (c["unsigned"]) {
          schema.min(0);
        }
        schema.max(maxP(c["precision"]));
        break;
      case "date":
      case "datetime":
        schema = yup.date();
        break;
      case "boolean":
        schema = yup.bool();
        break;
      case "time":
      case "blob":
      case "string":
      case "text":
      case "simple_array":
        schema = yup.string();
        break;
      default:
        throw `Unknown column type: ${c["type"]}`;
    }
  });
  return yup.object().shape(validations);
};

export const prepareFormConfig = (row, columns) => {
  // return an array of objects with structure:
  // {value:..., label:..., default:..., type: number|string|gis...}
  const entity = [];
  columns.forEach(col => {
    const field = col["field"];
    if (field === actionsCol) {
      return;
    }
    const e = {
      id: field,
      label: col["label"],
      default: col["default"],
      type: inputType(col),
      required: col.notnull === true
    };
    if (e.type === "select" || e.type === "multiple") {
      e.options = col.options || [];
    }
    if (e.required) {
      e.label += " *";
    }
    entity.push(e);
  });
  return entity;
};

export const onDateChange = (handleChange, field) => {
  return function(dates, value, flatpickr) {
    console.log(arguments);
    handleChange(field, value);
  };
};

export const prepareSelectOptions = (
  options = [],
  idField = "id",
  textField = "text"
) => {
  return options.map(o => {
    if (typeof o === "object") {
      if (o.hasOwnProperty("id")) {
        if (o.hasOwnProperty("text")) {
          // o is valid option
          return o;
        }
        if (o.hasOwnProperty(textField)) {
          // only text prop does not match
          return {
            id: o.id,
            text: o[textField]
          };
        }
        // text prop cannot be matched, use id
        return {
          id: o.id,
          text: o.id
        };
      } else if (o.hasOwnProperty(idField)) {
        if (o.hasOwnProperty("text")) {
          // custom id field
          return {
            id: o[idField],
            text: o.text
          };
        }
        if (o.hasOwnProperty(textField)) {
          // custom id field and text prop
          return {
            id: o[idField],
            text: o[textField]
          };
        }
        // text prop cannot be matched, use id
        return {
          id: o[idField],
          text: o[idField]
        };
      }
      throw `Cannot convert ${JSON.stringify(
        o
      )} to {id: id, text: text} format`;
    }
    if (isScalar(o)) {
      return { id: o, text: o.toString() };
    }

    throw `Unsupported option: ${JSON.stringify(
      o
    )}, supported simple strings, numbers, booleans and objects with defined id and text value`;
  });
};
