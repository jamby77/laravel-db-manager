const initialData = {
  tables: [],
  currentTable: {
    name: "",
    data: [],
    meta: {}
  }
};
initialData.tables = [
  "activity_log",
  "users",
  "data_rows",
  "data_types",
  "menu_items",
  "menus",
  "migrations",
  "password_resets",
  "permission_role",
  "permissions",
  "roles",
  "settings",
  "telescope_entries",
  "telescope_entries_tags",
  "telescope_monitoring",
  "translations",
  "user_roles",
  "sample_menu_items",
  "sample_menus",
  "sample_migrations",
  "sample_password_resets",
  "sample_permission_role",
  "sample_permissions",
  "sample_roles",
  "sample_settings",
  "sample_telescope_entries",
  "sample_telescope_entries_tags",
  "sample_telescope_monitoring",
  "sample_translations",
  "sample_user_roles"
];
initialData.currentTable = {
  name: "users",
  columns: [
    {
      label: "ID",
      field: "id",
      sortable: false,
      filterable: false,
      value: null,
      style: {}
    },
    {
      label: "Role ID",
      field: "role_id",
      sortable: false,
      filterable: false,
      value: null,
      style: {}
    },
    {
      label: "Name",
      field: "name",
      sortable: false,
      filterable: false,
      value: null,
      style: {}
    },
    {
      label: "Email",
      field: "email",
      sortable: false,
      filterable: false,
      value: null,
      style: {}
    },
    {
      label: "Avatar",
      field: "avatar",
      sortable: false,
      filterable: false,
      value: null,
      style: {}
    },
    {
      label: "Email Verified At",
      field: "email_verified_at",
      sortable: false,
      filterable: false,
      value: null,
      style: {}
    },
    {
      label: "Settings",
      field: "settings",
      sortable: false,
      filterable: false,
      value: null,
      style: {}
    },
    {
      label: "Created At",
      field: "created_at",
      sortable: false,
      filterable: false,
      value: null,
      style: {}
    },
    {
      label: "Updated At",
      field: "updated_at",
      sortable: false,
      filterable: false,
      value: null,
      style: {}
    }
  ],
  data: [
    {
      id: 1,
      role_id: 1,
      name: "Petar",
      email: "jamby77@gmail.com",
      avatar: "users/default.png",
      email_verified_at: null,
      settings: null,
      created_at: "2018-10-14 18:00:50",
      updated_at: "2018-10-14 18:00:51"
    },
    {
      id: 3,
      role_id: 2,
      name: "Test User",
      email: "test@email.com",
      avatar: "users/default.png",
      email_verified_at: null,
      settings: '{"locale":"en"}',
      created_at: "2018-10-14 18:07:25",
      updated_at: "2018-10-14 18:07:25"
    },
    {
      id: 9,
      role_id: 2,
      name: "Test User",
      email: "test@example.com",
      avatar: "users/default.png",
      email_verified_at: null,
      settings: null,
      created_at: "2018-10-21 20:37:39",
      updated_at: "2018-10-21 20:37:40"
    }
  ]
};

export default initialData;
