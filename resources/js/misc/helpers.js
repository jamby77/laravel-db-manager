export const fileSize = bytes => {
  if (bytes == 0) return "0 Bytes";
  const k = 1024,
    dm = 2,
    sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"],
    i = Math.floor(Math.log(bytes) / Math.log(k));
  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
};

let formatter;
export const formatNumber = (num, decimals) => {
  if (isNaN(Number(num))) {
    return num;
  }
  if (decimals && isNaN(Number(decimals))) {
    return num;
  }
  if (formatter === undefined) {
    formatter = new Intl.NumberFormat();
  }

  return formatter.format(num);
};

export const getRowKey = (row, idField) => {
  return idField
    .map(function(f) {
      return row[f];
    })
    .join("-");
};

export const getCellKey = (row, idField, col) => {
  return `${getRowKey(row, idField)}-${col.field}`;
};

export const isScalar = value => {
  return /boolean|number|string/.test(typeof value);
};
export const isEmpty = value => {
  if (null === value || void 0 === value) {
    return true;
  }
  if (typeof value === "string" && (value === "" || value.match(/^\s*$/))) {
    return true;
  }
  if (typeof value === "number" && isNaN(value)) {
    return true;
  }
  if (Array.isArray(value)) {
    if (value.length === 0) {
      return true;
    }
    const nonEmpty = value.filter(v => !isEmpty(v));
    return nonEmpty.length === 0;
  }

  return !!!value;
};
export const getCellValue = (row, col) => {
  if (col.value) {
    return col.value(row);
  }
  return isScalar(row[col.field])
    ? row[col.field]
    : JSON.stringify(row[col.field]); // todo use custom cells
};

export const actionsCol = "__actions__";

export const findRow = ({ data, keyField, id }) => {
  const row = data.find(row => {
    const rowId = getCellValue(row, { field: keyField });
    id = _.isNumber(rowId) ? parseInt(id) : id;
    return rowId === id;
  });
  return row || {};
};
export const delim = "...";
const pageSizes = {};

const generatePages = upTo => {
  upTo = parseInt(upTo, 10);
  if (isNaN(upTo)) {
    return [];
  }
  if (pageSizes[upTo]) {
    return pageSizes[upTo];
  }
  const pages = [];
  // fill in  the pages
  for (let i = 1; i <= upTo; i++) {
    pages.push(i);
  }
  pageSizes[upTo] = pages;
  return pages;
};

export const getPages = ({ lastPage, onEachSide, currentPage }) => {
  if (lastPage < 2) {
    return [];
  }
  const pagerWindow = onEachSide * 2;
  // show current + 2 on either side
  const allPages = generatePages(lastPage);
  if (allPages.length <= pagerWindow + 1) {
    return allPages;
  }

  const currentPageIdx = allPages.indexOf(currentPage); // 13 - 10 = 3
  if (currentPageIdx === -1) {
    return [];
  }
  let start,
    end,
    startIdx = pagerWindow - 2,
    endIdx = -2;

  if (currentPage <= pagerWindow || currentPage > lastPage - pagerWindow) {
    if (currentPage <= pagerWindow) {
      while (startIdx <= currentPageIdx) {
        startIdx++;
      }
    } else if (currentPage > lastPage - pagerWindow) {
      startIdx = 1;
      endIdx = -pagerWindow;
      while (currentPageIdx <= allPages.length + endIdx) {
        endIdx--;
      }
    }
    start = allPages.slice(0, startIdx);
    end = allPages.slice(endIdx);
    if (start[start.length - 1] + 1 === end[0]) {
      debugger;
      return [].concat(start, end);
    }
    return [].concat(start, delim, end);
  }
  start = allPages.slice(0, 1);
  end = allPages.slice(-2);

  const mid = allPages.slice(
    currentPageIdx - onEachSide,
    currentPageIdx + onEachSide + 1
  );
  return [].concat(start, delim, mid, delim, end);
};

export const showPageSizeOption = (size, total, allSizes, all = -1) => {
  return (
    (size !== all && total > size) || (total > allSizes[0] && size === all)
  );
};

export const getPrevPageNum = current_page =>
  current_page - 1 > 0 ? current_page - 1 : 1;

export const getNextPageNum = (current_page, last_page) =>
  current_page + 1 > last_page ? last_page : current_page + 1;
