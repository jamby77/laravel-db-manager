import React from "react";

const FlatpickrClasses = () => {
  return (
    <div className="flatpickr-calendar open static flatpickr-am-pm flatpickr-day flatpickr-days dayContainer hasTime hasWeeks inRange showTimeInput flatpickr-time flatpickr-time-separator flatpickr-innerContainer noCalendar rightMost arrowTop arrowBottom flatpickr-wrapper flatpickr-prev-month flatpickr-minute flatpickr-months">
      This file is not used anywhere, its purpose is to add flatpickr classes so
      that purge-css does not ignore them
    </div>
  );
};

export default FlatpickrClasses;
