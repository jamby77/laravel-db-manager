<?php
// TODO - 17.12.18 - add any general helper methods here

function api_prefix()
{
    $config = config('dbmanager');
    return array_get($config, 'route_group_config.prefix')
        . array_get($config, 'api.suffix') . '/'
        . array_get($config, 'api.tables_uri');
}

function dbconfig($key = null, $default = null) {
    if ($key === null) {
        return app(\Jamby77\DbManager\DbConfig::class);
    }

    return app(\Jamby77\DbManager\DbConfig::class)->get($key, $default);
}
