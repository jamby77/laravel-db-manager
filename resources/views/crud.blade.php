@extends('dbmanager::layout')
@section('head')
    @parent
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('vendor/dbmanager/css/dbmanager.css') }}" rel="stylesheet">

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('vendor/dbmanager/icons/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('vendor/dbmanager/icons/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('vendor/dbmanager/icons/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('vendor/dbmanager/icons/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('vendor/dbmanager/icons/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('vendor/dbmanager/icons/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('vendor/dbmanager/icons/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('vendor/dbmanager/icons/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('vendor/dbmanager/icons/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('vendor/dbmanager/icons/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('vendor/dbmanager/icons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('vendor/dbmanager/icons/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('vendor/dbmanager/icons/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('vendor/dbmanager/icons/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('vendor/dbmanager/icons/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">
    <!-- Script -->
    <script>
        var laraPMA = window.laraPMA || {};
        laraPMA.baseUrl = '{{route('dbmanager-api')}}';
        laraPMA.baseApiPath = '{{api_prefix()}}';
        laraPMA.basePath = '{{config('dbmanager.route_group_config.prefix')}}/{{config('dbmanager.api.tables_uri')}}';
        laraPMA.pagerHeight = 80;
        window.laraPMA = laraPMA;
    </script>
@endsection
@section('content')
    <div class="flex-center position-ref full-height h-100">
        <div id="app" class="content h-100"></div>
    </div>
    <script src="{{ mix('js/manifest.js', 'vendor/dbmanager') }}"></script>
    <script src="{{ mix('js/vendor.js', 'vendor/dbmanager') }}"></script>
    <script src="{{ mix('js/dbmanager.js', 'vendor/dbmanager') }}"></script>
@endsection
