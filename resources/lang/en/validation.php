<?php

return [
    'validation' => [
        'required' => 'Table rules do not allow ":attribute" field to be empty.',
    ]
];
