<?php

return [
    'route_group_config' => [
        'prefix' => 'dbmanager',
        'namespace' => 'Jamby77\\DbManager\\Controllers',
        'middleware' => ['web']
    ],
    'api' => [
        'tables_uri' => 'tables',
        'suffix' => '-api',
        'middleware' => ['api']
    ],
    'convertDbTypes' => [
        'boolean',
        'point',
        'set'
    ],
    'db_config_file' => 'dbmanager/config.json'
];
