<?php

namespace Jamby77\DbManager\Models;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Concerns\HasAttributes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Jamby77\DbManager\Database\Data\Processor;
use Jamby77\DbManager\Database\Validation\RuleGenerator;
use Jamby77\DbManager\Traits\SpatialTrait;
use PharIo\Manifest\Email;
use function PHPSTORM_META\type;

class TableData
{
    use SpatialTrait;
    use HasAttributes;
    public const ACTIONS_COL = '__actions__';
    public const CONTAINS = 'contains',
        DOES_NOT_CONTAIN = 'notContains',
        EMPTY = 'empty',
        ENDS_WITH = 'endsWith',
        EQUAL = 'eq',
        GREATER_THAN = 'gt',
        GREATER_THAN_OR_EQUAL = 'gte',
        LESS_THAN = 'lt',
        LESS_THAN_OR_EQUAL = 'lte',
        NOT_EMPTY = 'notEmpty',
        NOT_EQUAL = 'neq',
        STARTS_WITH = 'startsWith';
    protected $spatialFields = [];
    /**
     * @var string
     */
    private $tableName;
    /**
     * @var Table
     */
    private $table;
    private $row_actions = [
        'delete',
        'edit',
        'view',
        'copy',
    ];
    private $primaryCol;

    public function __construct(string $tableName)
    {
        $this->tableName = $tableName;
        $this->setGeometryColumns();
    }

    private function setGeometryColumns(): void
    {
        $gColumns = [
            'POINT',
            'LINESTRING',
            'POLYGON',
            'MULTIPOINT',
            'MULTILINESTRING',
            'MULTIPOLYGON',
            'GEOMETRYCOLLECTION',
        ];
        $columns = $this->tableDdl()->getRawColumns();
        $this->spatialFields = [];
        foreach ($columns as $column) {
            if (\in_array(strtoupper($column->Type), $gColumns, false)) {
                $this->spatialFields[] = $column->Field;
            }
        }
    }

    /**
     * @return Table
     */
    private function tableDdl(): Table
    {

        if (!$this->table) {
            $this->table = new Table($this->getTableName());
        }
        return $this->table;
    }

    /**
     * @return string
     */
    public function getTableName(): string
    {
        return $this->tableName;
    }

    public function getAllApi($params): array
    {
        $pageSize = array_get($params, 'pagesize', 10);
        $collection = $this->getAll($params);
        $data = $collection->toArray();
        $count = $collection->count();
        $paginatedData['current_page'] = 1;
        $paginatedData['last_page'] = 1;
        $paginatedData['from'] = 1;
        $paginatedData['per_page'] = $pageSize;
        $paginatedData['to'] = $count;
        $paginatedData['total'] = $count;
        $columns = $this->gridColumns();
        $this->addDataActionCol($data);
        return [
            'name' => $this->getTableName(),
            'keyField' => $this->getPrimaryCol(),
            'data' => $data,
            'pagination' => $paginatedData,
            'columns' => $columns,
            'meta' => $this->getMeta()
        ];
    }

    public function getAll(array $params = []): Collection
    {
        $results = $this->getQuery($params)->get();
        $this->transformForGrid($results);
        return $results;
    }

    /**
     * @param array $params
     * @return Builder
     */
    private function getQuery(array $params = []): Builder
    {
        $qb = $this->newBaseQueryBuilder()->from($this->getTableName());
        if (!empty($params)) {
            $sort = array_get($params, 'sorton');
            $sortDir = array_get($params, 'sortdir');
            unset($params['pagesize'], $params['page'], $params['sorton'], $params['sortdir']);// unset pager params, they are handled separately
            if ($sort !== null && ($sortDir === 'asc' || $sortDir === 'desc')) {
                $qb->orderBy($sort, $sortDir);
            }
            $this->addFilters($qb, $params);
        }
        return $qb;
    }

    private function addFilters(Builder $qb, array $filters = []): void
    {
        if (empty($filters)) {
            return;
        }
        foreach ($filters as $filter) {
            $filter = json_decode($filter);
            $op = $filter['op'];
            $field = $filter['field'];
            $value = $filter['value'];
            if (empty($op)) {
                $op = self::EQUAL;
            }
            switch ($op) {
                case self::EQUAL:
                    $qb->where($field, '=', $value);
                    break;
                case self::NOT_EQUAL:
                    break;
                case self::CONTAINS:
                    if (is_array($value)) {
                        $qb->whereIn($field, $value);
                    } else {
                        $qb->where($field, 'like', "%$value%");
                    }
                    break;
                case self::DOES_NOT_CONTAIN:
                    if (is_array($value)) {
                        $qb->whereNotIn($field, $value);
                    } else {
                        $qb->where($field, 'not like', "%$value%");
                    }
                    break;
                case self::EMPTY:
                    $qb->whereNull($field);
                    break;
                case self::NOT_EMPTY:
                    $qb->whereNotNull($field);
                    break;
                case self::ENDS_WITH:
                    $qb->where($field, 'like', "%$value");
                    break;
                case self::STARTS_WITH:
                    $qb->where($field, 'like', "$value%");
                    break;
                case self::LESS_THAN:
                    $qb->where($field, '<', $value);
                    break;
                case self::LESS_THAN_OR_EQUAL:
                    $qb->where($field, '<=', $value);
                    break;
                case self::GREATER_THAN:
                    $qb->where($field, '>', $value);
                    break;
                case self::GREATER_THAN_OR_EQUAL:
                    $qb->where($field, '>=', $value);
                    break;
            }
        }
    }

    private function transformForGrid(Collection $results): void
    {
        $results->transform(function ($result) {
            $this->setSpatialAttributes((array)$result);
            return $this->gridData();
        });
    }

    private function gridData(): array
    {
        $columns = $this->tableDdl()->getColumns();
        $rawData = $this->getAttributes();
        $resultData = [];
        foreach ($columns as $column) {
            $field = $column['name'];
            $fieldValue = $rawData[$field];
            if (is_array($fieldValue)) {
                $fieldValue = implode(', ', $fieldValue);
            }

            if ($fieldValue instanceof \DateTime) {
                $format = RuleGenerator::DATETIME_FORMAT;
                if ($column['type'] === 'time' || $column['type'] === 'time_immutable') {
                    $format = RuleGenerator::TIME_FORMAT;
                } elseif ($column['type'] === 'date' || $column['type'] === 'date_immutable') {
                    $format = RuleGenerator::DATE_FORMAT;
                }
                $fieldValue = $fieldValue->format($format);
            }

            if (is_resource($fieldValue)) {
                $str = fread($fieldValue, 64);
                $resultData[$field] = base64_encode($str) ?? '';
                continue;
            }
            if (!isset($fieldValue) || !$this->isStringColumn($column)) {
                $resultData[$field] = $fieldValue;
                continue;
            }
            if (mb_strlen($fieldValue) <= 64) {
                $resultData[$field] = $fieldValue;
                continue;
            }
            // TODO - 2018-11-25 - get wrap length from config/user config
            $wordwrap = wordwrap($fieldValue, 64, "\n", true);
            $explode = explode("\n", $wordwrap);
            $resultData[$field] = $explode[0] . '...';
        }
        return $resultData;
    }

    private function isStringColumn($column): bool
    {
        return \in_array(
            strtolower($column['type']),
            ['char', 'varchar', 'binary', 'varbinary', 'text', 'enum', 'string'],
            false);
    }

    private function dbConfig()
    {
        return dbconfig($this->getTableName(), []);
    }

    public function gridColumns(): array
    {
        $columns = $this->tableDdl()->getColumns();
        $girdColumns = [];
        foreach ($columns as $col) {
            $gridColDefinition = $this->gridColDefinition($col);
            $girdColumns[] = $gridColDefinition;
        }
        $this->addActionsCol($girdColumns);
        self::mergeConfig($girdColumns, $this->dbConfig());
        $this->updateDbConfig($girdColumns);
        return $girdColumns;
    }

    private function gridColDefinition(array $col): array
    {
        // TODO - 2018-11-11 - eventually lookup for stored config for this table and load it instead
        $definition = [
            'label' => ucwords(str_replace(['-', '_', '.'], ' ', $col['name'])),
            'field' => $col['name'],
            'sortable' => true,
            'filterable' => true,
            'visible' => true,
            'style' => []
        ];
        return array_merge($col, $definition);
    }

    private function addActionsCol(array &$gridColumns): void
    {
        if (!$this->row_actions) {
            return;
        }
        $gridColumns[] = [
            'label' => self::ACTIONS_COL,
            'field' => self::ACTIONS_COL,
            'sortable' => false,
            'filterable' => false,
            'value' => null,
            'style' => ['width' => '160px']
        ];
    }

    public function addDataActionCol(array &$data): void
    {
        foreach ($data as &$row) {
            $actions = $this->rowActions($row);
            if (\is_object($row)) {
                $row->{self::ACTIONS_COL} = $actions;
            } else {
                $row[self::ACTIONS_COL] = $actions;
            }
        }
    }

    private function rowActions($row): array
    {
        $primaryKey = $this->getPrimaryCol();
        $primaryData = [];
        if ($primaryKey) {
            foreach ($primaryKey as $col) {
                $primaryData[] = $row[$col];
            }
        } else {
            $primaryData = (array)$row;
        }
        $id = implode('-', $primaryData);
        return [
//            'comment' => [
//                'id' => $id,
//                'title' => 'Comment'
//            ],
            'view' => [
                'id' => $id,
                'title' => 'View'
            ],
            'edit' => [
                'id' => $id,
                'title' => 'Edit'
            ],
            'delete' => [
                'id' => $id,
                'title' => 'Delete'
            ],
        ];
    }

    /**
     * @return array|string|null
     */
    public function getPrimaryCol()
    {
        if (!$this->primaryCol) {
            $this->primaryCol = $this->tableDdl()->getPrimaryCol();
        }
        return $this->primaryCol;
    }

    public function getMeta()
    {
        return $this->tableDdl()->getAttribute('meta');
    }

    public function getPagedApi($params): array
    {
        $pageSize = array_get($params, 'pagesize', 10);
        $paginatedData = $this->getPaged($pageSize, $params)->toArray();
        $data = $paginatedData['data'] ?? [];
        unset($paginatedData['data']);

        $columns = $this->gridColumns();
        $this->addDataActionCol($data);
        return [
            'name' => $this->getTableName(),
            'keyField' => $this->getPrimaryCol(),
            'data' => $data,
            'pagination' => $paginatedData,
            'columns' => $columns,
            'meta' => $this->getMeta()
        ];
    }

    /**
     * @param $pageSize
     * @param array $params
     * @return LengthAwarePaginator
     */
    public function getPaged($pageSize, array $params = []): LengthAwarePaginator
    {
        /** @var \Illuminate\Pagination\LengthAwarePaginator $pagedResults */
        $pagedResults = $this->getQuery($params)->paginate($pageSize);
        $this->transformForGrid($pagedResults->getCollection());
        return $pagedResults;
    }

    public function delete(int $id): int
    {
        $primaryCol = $this->getPrimaryCol();
        $builder = $this->getQuery();
        foreach ($primaryCol as $col) {
            $builder->where($col, $id);
        }
        // delete record
        return $builder->delete();
    }

    public function truncate(): void
    {
        $this->getQuery()->truncate();
    }

    /**
     * @param array $record
     * @return array|Builder|mixed
     */
    public function addRecord(array $record)
    {
        $valid = $this->validate($record);
        if (empty($valid)) {
            throw new \InvalidArgumentException('Data is invalid');
        }
        $id = $this->getQuery()->insertGetId($valid);

        return $this->get($id);
    }

    private function validate(array $record): array
    {
        $columns = $this->tableDdl()->getColumns();
        $validationRules = RuleGenerator::generateValidationRules($columns);
        $validator = Validator::make($record, $validationRules, RuleGenerator::messages());
        $valid = $validator->validate();
        if (empty($valid)) {
            // maybe old laravel version
            $valid = $this->validated($record, $validationRules);
        }
        return Processor::prepareForDb($valid, $columns);
    }

    /**
     * Simplified version of \Illuminate\Validation\Validator::validated for older Laravel versions
     * @param array $record
     * @param array $validationRules
     * @return array
     */
    private function validated(array $record, array $validationRules): array
    {
        $results = [];

        $missingValue = Str::random(10);

        foreach (array_keys($validationRules) as $key) {
            $value = data_get($record, $key, $missingValue);

            if ($value !== $missingValue) {
                Arr::set($results, $key, $value);
            }
        }
        return $results;
    }

    /**
     * @param $id
     * @return Builder|mixed
     */
    public function get($id): array
    {
        $builder = $this->getQuery();
        $this->byId($id, $builder);
        $result = $builder->first();
        if ($this->spatialFields) {
            $this->setSpatialAttributes((array)$result);
        } else {
            $this->setRawAttributes((array)$result);
        }
        return $this->getAttributes();
    }

    /**
     * @param $id
     * @param Builder $builder
     */
    private function byId($id, Builder $builder): void
    {
        $primaryCol = $this->getPrimaryCol();
        foreach ($primaryCol as $col) {
            $builder->where($col, $id);
        }
    }

    public function setRawAttributes(array $attributes, $sync = false)
    {
        $columns = $this->tableDdl()->getDoctrineColumns();
        $doctrineConnection = $this->getConnection()->getDoctrineConnection();
        $platform = $doctrineConnection->getDatabasePlatform();
        foreach ($columns as $c) {
            $name = $c->getName();
            if (isset($attributes[$name])) {
                $type = $c->getType();
                $typeName = $type->getName();
                if ($this->shouldConvertToPhp($typeName)) {
                    // do something here
                    $value = $type->convertToPHPValue($attributes[$name], $platform);
                    $attributes[$name] = $value;
                }
            }
        }

        $this->attributes = $attributes;

        if ($sync) {
            $this->syncOriginal();
        }
        return $this;
    }

    public function getConnection()
    {
        return DB::connection();
    }

    /**
     * @param string $typeName
     * @return bool
     */
    private function shouldConvertToPhp(string $typeName): bool
    {
        return in_array($typeName, config('dbmanager.convertDbTypes'), false);
    }

    public function updateRecord(int $id, array $record)
    {
        $valid = $this->validate($record);
        if (empty($valid)) {
            throw new \InvalidArgumentException('Data is invalid');
        }
        $builder = $this->getQuery();
        $this->byId($id, $builder);
        $builder->update($valid);
        return $this->get($id);
    }

    public static function mergeConfig(array &$destConfig, array $srcConfig): void
    {
        foreach ($destConfig as &$column) {
            $name = $column['field'];
            $dbCols = array_filter($srcConfig, function ($c) use ($name) {
                return $c['field'] === $name;
            });
            if (!empty($dbCols)) {
                $column = array_merge_recursive($column, array_shift($dbCols));
            }
        }
    }

    public function updateDbConfig(array $config): array
    {
        $dbConfig = dbconfig();
        if ($dbConfig) {
            $dbConfig->put($this->getTableName(), $config);
        }
        return $config;
    }
}
