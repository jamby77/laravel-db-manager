<?php

namespace Jamby77\DbManager\Models;

use ArrayAccess;
use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Schema\Index;
use Doctrine\DBAL\Schema\Table as DoctrineTable;
use Illuminate\Contracts\Queue\QueueableEntity;
use Illuminate\Contracts\Routing\UrlRoutable;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Support\Facades\DB;
use Jamby77\DbManager\Database\Types\EnumType;
use Jamby77\DbManager\Database\Types\SetType;
use JsonSerializable;

class Table implements ArrayAccess, Arrayable, Jsonable, JsonSerializable, QueueableEntity, UrlRoutable
{
    private static $tables;
    private $attributes;
    private $tableName;
    private $primaryKey;
    private $rawColumns = [];
    private $primaryCol;
    private $columns;
    private $doctrineColumns;

    public function __construct(string $tableName, array $attributes = [])
    {
        $this->tableName = $tableName;
        if (empty($attributes)) {
            $attributes = $this->findTable();
        }
        $this->attributes = $attributes;
    }

    public static function listTables()
    {
        return self::getTables();
    }

    /**
     * @return array
     */
    private function findTable(): array
    {
        $tables = array_filter(self::listTables(), function ($table) {
            return $table['name'] === $this->tableName;
        });
        return (array)array_shift($tables);
    }

    public static function dbName()
    {
        return DB::connection()->getDatabaseName();
    }

    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }

    public function getConnection()
    {
        return DB::connection();
    }

    /**
     * @return array
     */
    private static function getTables(): array
    {
        if (!static::$tables) {
            $tablesWithDetails = DB::connection()->getDoctrineSchemaManager()->listTables();
            static::$tables = array_map(function (DoctrineTable $table) {
                return [
                    'name' => $table->getName(),
                    'columns' => array_values(array_map(function (Column $col) {
                        $details = $col->toArray();
                        $details['type'] = $col->getType()->getName();
                        return $details;
                    }, $table->getColumns())),
                    'indexes' => $table->getIndexes(),
                    'meta' => self::loadTableMeta($table->getName()),
                ];
            }, $tablesWithDetails);
        }
        return self::$tables;
    }

    public static function loadTableMeta(string $tableName)
    {
        $meta = [];
        $pdo = DB::getPdo();
        $query = $pdo->prepare('SELECT * FROM `information_schema`.`tables` t WHERE t.`table_name`=:table AND t.table_schema =:db');
        try {
            $query->execute(['table' => $tableName, 'db' => DB::getDatabaseName()]);
            $result = $query->fetchAll(\PDO::FETCH_ASSOC);
            if ($result) {
                $meta = array_change_key_case(array_shift($result));
            }
        } catch (\Exception $e) {
        }
        return $meta;
    }

    /**
     * @param array $tables
     */
    public static function setTables(array $tables): void
    {
        self::$tables = $tables;
    }

    public function toArray()
    {
        return $this->attributes;
    }

    public function offsetExists($offset)
    {
        return isset($this->attributes[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->attributes[$offset];
    }

    public function offsetSet($offset, $value)
    {
        $this->attributes[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->attributes[$offset]);
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    public function getQueueableId()
    {
        return 'name';
    }

    public function getQueueableRelations()
    {
        return [];
    }

    public function getQueueableConnection()
    {
        return null;
    }

    /**
     * Get the value of the model's route key.
     *
     * @return mixed
     */
    public function getRouteKey()
    {
        return $this->getAttribute($this->getRouteKeyName());
    }

    public function getRouteKeyName()
    {
        return $this->getKeyName();
    }

    /**
     * Get the primary key for the table.
     *
     * @return string
     */
    public function getKeyName(): string
    {
        return $this->primaryKey;
    }

    /**
     * Set the primary key for the table.
     *
     * @param  string $key
     * @return $this
     */
    public function setKeyName($key): self
    {
        $this->primaryKey = $key;

        return $this;
    }

    /**
     * Retrieve the model for a bound value.
     *
     * @param  mixed $value
     * @return static|null
     */
    public function resolveRouteBinding($value)
    {
        return new static($value);
    }

    public function getAttribute(string $attribute)
    {
        return $this->attributes[$attribute];
    }

    /**
     * Handle dynamic static method calls into the method.
     *
     * @param  string $method
     * @param  array $parameters
     * @return mixed
     */
    public static function __callStatic($method, $parameters)
    {
        return (new static(''))->$method(...$parameters);
    }

    /**
     * @return \stdClass[]
     */
    public function getRawColumns(): array
    {
        if (empty($this->rawColumns[$this->tableName])) {
            $connection = DB::connection();
            $tableColumnsSQL = $connection
                ->getDoctrineSchemaManager()
                ->getDatabasePlatform()
                ->getListTableColumnsSQL($this->tableName, $connection->getDatabaseName());
            $this->rawColumns[$this->tableName] = $connection->select($tableColumnsSQL);
        }
        return $this->rawColumns[$this->tableName];
    }

    public function getColumns()
    {
        if ($this->columns === null) {
            $columns = $this->getAttribute('columns');
            $raw = $this->getRawColumns();
            foreach ($columns as &$column) {
                if (!in_array($column['type'], [EnumType::NAME, SetType::NAME], true)) {
                    continue;
                }

                foreach ($raw as $rawCol) {
                    if ($column['name'] === $rawCol->Field) {
                        $type = $rawCol->Type;
                        if (starts_with($type, 'enum(') && ends_with($type, ')')) {
                            $column['options'] = explode("','", trim(substr($type, strlen('enum('), -1), "'"));
                        } elseif (starts_with($type, 'set(') && ends_with($type, ')')) {
                            $column['options'] = explode("','", trim(substr($type, strlen('set('), -1), "'"));
                        }
                    }
                }
            }
            unset($column);
            $this->columns = $columns;
        }
        return $this->columns;
    }

    public function getPrimaryCol()
    {
        if (!$this->primaryCol) {
            $indexes = $this->getAttribute('indexes');
            if (!$indexes) {
                return null;
            }
            /** @var Index $index */
            foreach ($indexes as $index) {
                if ($index->isPrimary()) {
                    $this->primaryCol = $index->getColumns();
                }
            }
        }
        return $this->primaryCol;
    }

    /**
     * @return Column[]
     */
    public function getDoctrineColumns(): array
    {
        if (!$this->doctrineColumns) {
            $this->doctrineColumns = DB::getDoctrineSchemaManager()->listTableDetails($this->tableName)->getColumns();
        }
        return $this->doctrineColumns;
    }
}
