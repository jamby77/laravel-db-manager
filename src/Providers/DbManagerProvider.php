<?php

namespace Jamby77\DbManager\Providers;

use Doctrine\DBAL\Types\Type;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Jamby77\DbManager\Database\Types\BitType;
use Jamby77\DbManager\Database\Types\EnumType;
use Jamby77\DbManager\Database\Types\SetType;
use Jamby77\DbManager\DbConfig;
use Jamby77\DbManager\Models\TableData;

class DbManagerProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->registerHelpers();
        // load resources, register routes
        $this->loadViews();
        $this->registerRoutes();
        $this->publishConfiguration();
        $this->publishAssets();
//        $this->registerCommands();// maybe at some point will add some helper commands
        $this->registerDatabaseLoader();
        $this->loadLocaleMessages();
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        // register bindings , merge configs
        $this->mergeConfiguration();
        $this->registerDbMappings();
        $this->registerDbConfig();
    }

    private function loadViews(): void
    {
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'dbmanager');
        $this->publishes([
            __DIR__ . '/../../resources/views' => resource_path('views/vendor/dbmanager')
        ], 'views');
    }

    private function loadLocaleMessages(): void
    {
        $this->loadTranslationsFrom(__DIR__ . '/../../resources/lang/', 'dbmanager');
        $this->publishes([
            __DIR__ . '/../../resources/lang' => resource_path('lang/vendor/dbmanager')
        ], 'locale');
    }

    private function registerRoutes(): void
    {
        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
        $this->loadRoutesFrom(__DIR__ . '/../../routes/api.php');
    }

    private function publishConfiguration(): void
    {
        $this->publishes([
            __DIR__ . '/../../config/dbmanager.php' => config_path('dbmanager.php'),
        ], 'config');
    }

    private function mergeConfiguration(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../../config/dbmanager.php', 'dbmanager');
    }

    private function publishAssets(): void
    {
        $this->publishes([
            __DIR__ . '/../../public/assets' => public_path('vendor/dbmanager'),
        ], 'assets');

    }

    private function registerHelpers(): void
    {
        require __DIR__ . '/../../resources/helpers.php';
    }

    private function registerDatabaseLoader(): void
    {
        // TODO - 17.12.18 - register db manager here, if it seems to be appropriate at some stage
        // @see https://dev.mysql.com/doc/refman/5.7/en/identifiers.html
        $mysqlIdentifierRegex = '[a-z,A-Z][0-9,a-z,A-Z$_]{0,64}';
        Route::pattern('table', $mysqlIdentifierRegex);
        Route::bind('table', function ($value) {
            return new TableData($value);
        });
    }

    private function registerDbMappings(): void
    {
//        $this->app->register(DoctrineSupportServiceProvider::class);
        $connection = \DB::connection();
        $platform = $connection->getDoctrineConnection()->getDatabasePlatform();
        $types = [
            'bit' => BitType::class,
            'enum' => EnumType::class,
            'set' => SetType::class
        ];
        foreach ($types as $type => $handler) {
            if (!Type::hasType($type)) {
                Type::addType($type, $handler);
            }

            $platform->registerDoctrineTypeMapping($type, $type);
        }
    }

    private function registerDbConfig(): void
    {
        $this->app->singleton(DbConfig::class, function() {
            $dbConfigPath = storage_path(config('dbmanager.db_config_file'));
            if (!is_dir(dirname($dbConfigPath))) {
                \File::makeDirectory(dirname($dbConfigPath), 0755, true);
            }
            return DbConfig::make($dbConfigPath);
        });
    }
}
