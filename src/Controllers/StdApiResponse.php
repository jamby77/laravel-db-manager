<?php
/**
 * Created by PhpStorm.
 * User: pp
 * Date: 2018-10-28
 * Time: 11:42
 */

namespace Jamby77\DbManager\Controllers;

class StdApiResponse implements \JsonSerializable
{
    public const SUCCESS = 'success',
        ERROR = 'error';
    protected $status = '';
    protected $message = '';
    protected $data = [];

    public function __construct($status, $message = '', $data = [])
    {
        $this->status = $status;
        $this->message = $message;
        $this->data = $data;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function __set($name, $value)
    {
        if ($name === 'status') {
            $this->status = $value;
        } elseif ($name === 'message') {
            $this->message = $value;
        } elseif ($name === 'data') {
            $this->data = $value;
        } else {
            throw new \InvalidArgumentException('Unsupported data');
        }
        return $this;
    }

    public function __isset($name)
    {
        return $name === 'status' || $name === 'message' || $name === 'data';
    }

    public function __get($name)
    {
        if ($name === 'status') {
            return $this->status;
        }

        if ($name === 'message') {
            return $this->message;
        }

        if ($name === 'data') {
            return $this->data;
        }

        throw new \InvalidArgumentException('Unsupported data');
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'status' => $this->status,
            'message' => $this->message,
            'data' => $this->data,
        ];
    }
}
