<?php

namespace Jamby77\DbManager\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Jamby77\DbManager\Models\TableData;

class TableDataController extends Controller
{
    /**
     * Display a listing of table rows paginated
     *
     * @param TableData $table
     * @return array
     */
    public function index(TableData $table): array
    {
        $pageSize = (int)request()->get('pagesize', 10);
        $params = request()->all();
        if ($pageSize === -1) {
            return $table->getAllApi($params);
        }
        return $table->getPagedApi($params);
    }

    /**
     * Display row id
     *
     * @param TableData $table
     * @param  int $id
     * @return array
     */
    public function show(TableData $table, $id): array
    {
        return (array)$table->get($id);
    }

    /**
     * Add a row in table
     *
     * @param  \Illuminate\Http\Request $request
     * @param TableData $table
     * @return array
     */
    public function store(Request $request, TableData $table): array
    {
        return $table->addRecord($request->all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param TableData $table
     * @param  int $id
     * @return array|\Illuminate\Database\Query\Builder|mixed
     */
    public function update(Request $request, TableData $table, $id)
    {
        return $table->updateRecord($id, $request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param TableData $table
     * @param  int $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function destroy(TableData $table, $id)
    {
        try {
            $table->delete($id);
            return response()->make(null)->setStatusCode(200, 'Deleted');
        } catch (\Exception $e) {
            return response()->make(null)->setStatusCode(404, 'Record not found');
        }
    }

    /**
     * Drop / truncate a table
     *
     * @param TableData $table
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function truncate(TableData $table)
    {
        try {
            $table->truncate();
            return response()->setStatusCode(201);
        } catch (\Exception $e) {
            return response()->setStatusCode(404);
        }
    }
}
