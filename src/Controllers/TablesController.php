<?php

namespace Jamby77\DbManager\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\{JsonResponse, Request, Response};
use Illuminate\Support\Facades\Schema;
use Jamby77\DbManager\Models\Table;
use Jamby77\DbManager\Models\TableData;

class TablesController extends Controller
{
    /**
     * Display a listing of all accessible tables.
     */
    public function index(): array
    {
        return ['tables' => Table::listTables(), 'dbname' => Table::dbName()];
    }

    /**
     * Display the specified resource.
     *
     * @param TableData $table
     * @return JsonResponse
     */
    public function show($table): JsonResponse
    {
        return \response()->json(new Table($table->getTableName()));
    }

    /**
     * Create a table
     *
     * @param  \Illuminate\Http\Request $request
     * @return Response
     */
    public function store(Request $request): ?Response
    {
        $definition = $request->only(['definition']);
        if (!isset($definition['table'])) {
            \response()->json(new StdApiResponse(StdApiResponse::ERROR,
                'Table name is required'), 400);
        }
        Schema::create($definition['table'], function (Blueprint $table) use ($definition) {
            foreach ($definition as $columnDefinfition) {
                // todo loop submitted definition and create corresponding column
            }
        });

        response()->json(new StdApiResponse(StdApiResponse::SUCCESS,
            "Table {$definition['table']} created"), 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Table $table
     * @return Response
     */
    public function update(Request $request, Table $table): ?Response
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Table $table
     * @return Response
     */
    public function destroy(Table $table): ?Response
    {
        //
    }
}
