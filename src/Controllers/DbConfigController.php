<?php

namespace Jamby77\DbManager\Controllers;

use App\Http\Controllers\Controller;
use Jamby77\DbManager\Models\TableData;

class DbConfigController extends Controller
{
    /**
     * @param TableData|null $table
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(TableData $table = null)
    {
        $result = $table ? $table->gridColumns() : \dbconfig()->all();
        return response()->json($result);
    }

    public function update(TableData $table = null)
    {
        $config = request()->all();
        if (!is_array($config)) {
            throw new \InvalidArgumentException('Invalid config');
        }
        if ($table) {
            $result = $table->updateDbConfig($config);
        } else {
            $result = \dbconfig()->put($config)->all();
        }
        return response()->json($result);
    }
}
