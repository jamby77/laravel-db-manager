<?php

namespace Jamby77\DbManager\Database\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use Illuminate\Support\Facades\DB;

class SetType extends Type
{
    public const NAME = 'set';

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        $allowed = array_get($fieldDeclaration, 'options', []);

        $pdo = DB::connection()->getPdo();

        // trim the values
        $fieldDeclaration['allowed'] = array_map(function ($value) use ($pdo) {
            return $pdo->quote(trim($value));
        }, $allowed);

        return 'SET(' . implode(', ', $fieldDeclaration['allowed']) . ')';
    }

    /**
     * Gets the name of this type.
     *
     * @return string
     */
    public function getName()
    {
        return self::NAME;
    }

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return array|mixed
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return explode(',', $value);
    }

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return mixed|string
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (!is_array($value)) {
            $value = (array)$value;
        }
        return implode(',', $value);
    }
}
