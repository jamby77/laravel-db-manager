<?php
/**
 * Created by PhpStorm.
 * User: pp
 * Date: 2018-12-23
 * Time: 15:46
 */

namespace Jamby77\DbManager\Database\Data;

class Processor
{
    public static function prepareForDb($record, $columns): array
    {
        $data = [];
        foreach ($columns as $c) {
            $auto = $c['autoincrement'];
            $name = $c['name'];
            $type = $c['type'];
            if ($auto) {
                continue;
            }
            if (!isset($record[$name])) {
                continue;
            }
            $value = $record[$name];

            switch ($type) {
                case 'set':
                    $data[$name] = is_array($value) ? implode(',', $value) : $value;
                    break;
                default:
                    $data[$name] = $value;
            }
        }
        return $data;
    }
}
