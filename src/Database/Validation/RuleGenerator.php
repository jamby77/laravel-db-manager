<?php

namespace Jamby77\DbManager\Database\Validation;

use Doctrine\DBAL\Schema\Column;

/**
 * Created by PhpStorm.
 * User: pp
 * Date: 2018-12-23
 * Time: 13:46
 */
class RuleGenerator
{
    public const DATE_FORMAT = 'Y-m-d';
    public const TIME_FORMAT = 'H:i:s';
    public const DATETIME_FORMAT = 'Y-m-d H:i:s';

    public static $errorMessages;

    public static function messages()
    {
        if (!self::$errorMessages) {
            self::$errorMessages = app('translator')->get('dbmanager::validation.validation');
        }
        return self::$errorMessages;
    }

    /**
     * Generate validation rules after database declaration rules
     * @param Column[] $columns
     * @return array
     */
    public static function generateValidationRules(array $columns = []): array
    {
        $rules = [];
//        dump($columns);
        foreach ($columns as $c) {
            $auto = $c['autoincrement'];
            if ($auto) {
                continue;
            }
            $name = $c['name'];
            $type = $c['type'];
            $required = $c['notnull'];
            $maxLength = $c['length'];
            if ($type === 'text' && ($maxLength === 0 || !$maxLength)) {
                $maxLength = 65535;
            }
            $minLength = $required ? 1 : 0;
            $maxSize = $c['precision'];
            $positive = $c['unsigned'];
            $default = $c['default'];
            $r = [];
            if ($required) {
                $r[] = 'required';
            } elseif ($default !== null) {
                $r[] = 'nullable';
            }
            switch ($type) {
                case 'datetime':
                case 'datetime_immutable':
                    $r[] = 'date_format:' . self::DATETIME_FORMAT;
                    break;
                case 'date':
                case 'date_immutable':
                    $r[] = 'date_format:' . self::DATE_FORMAT;
                    break;
                case 'datetimetz':
                case 'datetimetz_immutable':
                    $r[] = 'date_format:' . self::DATETIME_FORMAT;
                    break;
                case 'time':
                case 'time_immutable':
                    $r[] = 'date_format:' . self::TIME_FORMAT;
                    break;
                case 'dateinterval':
                    $r[] = 'digits';
                    break;
                case 'boolean':
                    $r[] = 'boolean';
                    break;
                case 'smallint':
                    $min = $positive ? 0 : -32768;
                    $max = $positive ? 65535 : 32767;
                    $r[] = 'integer';
                    $r[] = "between:${min},${max}";
                    break;
                case 'integer':
                    $min = $positive ? 0 : -2147483648;
                    $max = $positive ? 4294967295 : 2147483647;
                    $r[] = 'integer';
                    $r[] = "between:${min},${max}";
                    break;
                case 'bigint':
                    $r[] = 'integer';
                    if ($positive) {
                        $maxSize++;
                    }
                    $r[] = "digits_between:1,${maxSize}";
                    break;
                case 'decimal':
                case 'float':
                    $r[] = 'numeric';
                    break;
                case 'string':
                case 'text':
                case 'guid':
                    $r[] = 'string';
                    $r[] = isset($c['fixed']) && $c['fixed'] ? "size:$maxLength" : "between:$minLength,$maxLength";
                    break;
                case 'enum':
                    $r[] = 'in:' . implode(',', $c['options']);
                    break;
                case 'set':
                    $options = $c['options'];
                    $r[] = function ($attribute, $value, $fail) use ($options) {
                        // valid set value is either '', one or more valid options
                        if ($value === '') {
                            // empty string is valid SET value
                            return;
                        }
                        if (is_scalar($value) && !in_array($value, $options, false)) {
                            $fail("$value is not valid value for $attribute");
                        } elseif (is_array($value)) {
                            //validate array
                            $invalid = array_filter($value, function ($o) use ($options) {
                                return !in_array($o, $options, false);
                            });
                            if ($invalid) {
                                $fail(implode(', ', $invalid) . ' are not valid SET values for ' . $attribute);
                            }
                        } elseif (!is_scalar($value) && !is_array($value)) {
                            $fail("$attribute should be either string ar array");
                        }
                    };
                    $rules[$name] = $r;
                    continue 2;
                    break;
            }

            $rules[$name] = implode('|', $r);
        }
//        dd($rules);
        return $rules;
    }

}
