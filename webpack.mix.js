let mix = require("laravel-mix");
const path = require("path");
require("laravel-mix-purgecss");
const DashboardPlugin = require("webpack-dashboard/plugin");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.setPublicPath("public/assets");
// mix.setPublicPath("../../../public/vendor/dbmanager");
mix
  .webpackConfig({
    plugins: [new DashboardPlugin()]
  })
  .react("resources/js/dbmanager.js", "js")
  .extract()
  .sass("resources/sass/dbmanager.scss", "css")
  .purgeCss({
    enabled: true,
    globs: [
      path.join(__dirname, "node_modules/flatpickr/**/*.js"),
      path.join(__dirname, "resources/views/**/*.php"),
      path.join(__dirname, "resources/js/**/*.js")
    ],
    whitelist: ["page-loader", "last-child", "data-reach-menu-item"]
  });
if (mix.inProduction()) {
  mix.version();
} else {
  mix.sourceMaps();
}
