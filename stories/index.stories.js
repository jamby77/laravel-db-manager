import React from "react";

import { storiesOf } from "@storybook/react";
import "../public/assets/css/dbmanager.css";
import { BaseTableStory, NoTableStory } from "./table/TableStories";
import {
  FivePages,
  GoToPageStory,
  PageSizes,
  PaginationStory,
  SixPages,
  TenPages
} from "./table/pagination/PaginationStories";
import { PlainFormStory } from "./form/FormStories";
import {
  LongListStory,
  MultiSelectItemAddonStory,
  MultiSelectItemStory,
  MultiSelectMenuStory,
  MultiSelectMenuWithSearchStory,
  MultiSelectRowStory,
  MultiSelectSearchInputStory,
  SimpleList
} from "./form/components/lists/ListsStories";
import {
  BaseFilterStory,
  BaseFilterWithPropsStory,
  CheckboxFilterStory,
  NumberFilterStory
} from "./table/filters/FilterStories";
import { withKnobs } from "@storybook/addon-knobs";
import { MainConfigStory } from "./table/config/ConfigStories";

storiesOf("Table", module)
  .addDecorator(withKnobs)
  .add("Empty table", () => <NoTableStory />)
  .add("Base table", () => <BaseTableStory />)
  .add("Base filter", () => <BaseFilterStory />)
  .add("Base filter with props", () => <BaseFilterWithPropsStory />)
  .add("Number filter", () => <NumberFilterStory />)
  .add("Checkbox filter", () => <CheckboxFilterStory />);

storiesOf("Table config", module).add("Main config", () => <MainConfigStory />);

storiesOf("Pagination", module)
  .add("Page sizes", () => <PageSizes />)
  .add("Pages up to 5", () => <FivePages />)
  .add("Pages up to 6", () => <SixPages />)
  .add("Pages up to 10", () => <TenPages />)
  .add("Go to page", () => <GoToPageStory />)
  .add("Pagination", () => <PaginationStory />);

storiesOf("EntityForm", module).add("plain", () => <PlainFormStory />);

storiesOf("Selects and autocompletes", module)
  .add("simple list", () => <SimpleList />)
  .add("looong list", () => <LongListStory />);
storiesOf("multi select elements", module)
  .add("multi select item", () => <MultiSelectItemStory />)
  .add("multi select addon", () => <MultiSelectItemAddonStory />)
  .add("multi select row", () => <MultiSelectRowStory />)
  .add("multi select menu", () => <MultiSelectMenuStory />)
  .add("multi select menu search input", () => <MultiSelectSearchInputStory />)
  .add("multi select menu with search", () => (
    <MultiSelectMenuWithSearchStory />
  ));
