import React from "react";
import { MemoryRouter as Router } from "react-router-dom";
import Sizes from "../../../resources/js/components/common/pagination/Sizes";
import { getPages } from "../../../resources/js/misc/helpers";
import Pages from "../../../resources/js/components/common/pagination/Pages";
import GotoPage from "../../../resources/js/components/common/pagination/GotoPage";
import { Pagination } from "../../../resources/js/components/common/PaginationController";

export const PageSizes = () => {
  return (
    <Router>
      <div>
        <Sizes
          from={1}
          to={12}
          currentPageSize={10}
          pageSizeOptions={[10, 20, 50, 100, -1]}
          setPagination={() => {}}
          total={23}
        />
      </div>
    </Router>
  );
};

export const FivePages = () => {
  const currentPages = [1, 2, 3, 4, 5];
  return (
    <Router>
      <div>
        {currentPages.map(p => {
          const pages = getPages({
            lastPage: currentPages[currentPages.length - 1],
            onEachSide: 2,
            currentPage: p
          });
          console.log(pages);
          return (
            <div key={p}>
              <Pages
                pages={pages}
                setPagination={() => {}}
                currentPage={p}
                lastPage={currentPages[currentPages.length - 1]}
              />
            </div>
          );
        })}
      </div>
    </Router>
  );
};

export const SixPages = () => {
  const currentPages = [1, 2, 3, 4, 5, 6];
  return (
    <Router>
      <div>
        {currentPages.map(p => {
          const pages = getPages({
            lastPage: currentPages[currentPages.length - 1],
            onEachSide: 2,
            currentPage: p
          });
          console.log(pages);
          return (
            <div key={p}>
              <Pages
                pages={pages}
                setPagination={() => {}}
                currentPage={p}
                lastPage={currentPages[currentPages.length - 1]}
              />
            </div>
          );
        })}
      </div>
    </Router>
  );
};

export const TenPages = () => {
  const currentPages = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  return (
    <Router>
      <div>
        {currentPages.map(p => {
          const pages = getPages({
            lastPage: currentPages[currentPages.length - 1],
            onEachSide: 2,
            currentPage: p
          });
          console.log(pages);
          return (
            <div key={p}>
              <Pages
                pages={pages}
                setPagination={() => {}}
                currentPage={p}
                lastPage={currentPages[currentPages.length - 1]}
              />
            </div>
          );
        })}
      </div>
    </Router>
  );
};

export const GoToPageStory = () => {
  return (
    <Router>
      <div>
        <GotoPage
          onPageChange={page => console.log(page)}
          firstPage={2}
          lastPage={5}
        />
      </div>
    </Router>
  );
};

export const PaginationStory = () => {
  return (
    <Router>
      <div>
        <Pagination
          table={"Test"}
          pages={[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
          from={10}
          to={20}
          total={150}
          currentPageSize={10}
          pageSizeOptions={[10, 20, 50, 100, -1]}
          setPagination={() => {}}
          currentPage={10}
          lastPage={10}
          showGotoPage
        />
      </div>
    </Router>
  );
};
