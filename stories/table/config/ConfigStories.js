import React from "react";
import MainConfig from "../../../resources/js/components/crud/config/MainConfig";
import { table } from "../TableStories";

export const MainConfigStory = () => {
  return <MainConfig table={table} />;
};
