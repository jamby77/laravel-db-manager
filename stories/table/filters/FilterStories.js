import React from "react";
import ColumnFilter from "../../../resources/js/components/crud/filters/ColumnFilter";
import { text } from "@storybook/addon-knobs";
import { FilterTypes } from "../../../resources/js/components/crud/filters/constants";

export const NumberFilterStory = () => {
  return (
    <div style={{ width: "200px" }}>
      <ColumnFilter type={FilterTypes.NUMBER} />
      <ColumnFilter type={FilterTypes.NUMBER} min={1} max={10} />
      <ColumnFilter type={FilterTypes.NUMBER} min={0} max={10} step={0.1} />
    </div>
  );
};

export const CheckboxFilterStory = () => {
  return (
    <div style={{ width: "200px" }}>
      <ColumnFilter type={FilterTypes.CHECKBOX} />
    </div>
  );
};

export const BaseFilterStory = () => {
  return (
    <div style={{ width: "200px" }}>
      <ColumnFilter />
    </div>
  );
};

export const BaseFilterWithPropsStory = () => {
  const placeholder = text("Placeholder", "Plain search");
  let i = 1;
  return [
    <ColumnFilter placeholder={placeholder} key={i++} />,
    <ColumnFilter placeholder={placeholder} defaultValue={"test"} key={i++} />,
    <ColumnFilter
      key={i}
      placeholder={text("Placeholder 2", "Search with onChange")}
      onChange={v => console.log("Custom change handler", v)}
    />
  ];
};
