import React from "react";
import Table from "../../resources/js/components/crud/Table";
import cn from "classnames";
import { MemoryRouter as Router } from "react-router-dom";

const containerStyle = { height: `${500}px` };
const containerClasses = cn(["table-container", "p-lg-3"]);
const defaultProps = { containerClasses, containerStyle };
export const NoTableStory = () => {
  return (
    <Router>
      <Table {...defaultProps} table={{}} rowKey={1} />
    </Router>
  );
};

export const table = {
  name: "customers",
  keyField: ["customerNumber"],
  data: [
    {
      customerNumber: 103,
      customerName: "Atelier graphique",
      contactLastName: "Schmitt",
      contactFirstName: "Carine ",
      phone: "40.32.2555",
      addressLine1: "54, rue Royale",
      addressLine2: null,
      city: "Nantes",
      state: null,
      postalCode: "44000",
      country: "France",
      salesRepEmployeeNumber: 1370,
      creditLimit: 21000,
      avatar: "",
      photo: "",
      attach: "",
      sex: "male",
      interests: "",
      __actions__: {
        view: {
          id: "103",
          title: "View"
        },
        edit: {
          id: "103",
          title: "Edit"
        },
        delete: {
          id: "103",
          title: "Delete"
        }
      }
    },
    {
      customerNumber: 112,
      customerName: "Signal Gift Stores",
      contactLastName: "King",
      contactFirstName: "Jean",
      phone: "7025551838",
      addressLine1: "8489 Strong St.",
      addressLine2: null,
      city: "Las Vegas",
      state: "NV",
      postalCode: "83030",
      country: "USA",
      salesRepEmployeeNumber: 1166,
      creditLimit: 71800,
      avatar: "",
      photo: "",
      attach: "",
      sex: "male",
      interests: "",
      __actions__: {
        view: {
          id: "112",
          title: "View"
        },
        edit: {
          id: "112",
          title: "Edit"
        },
        delete: {
          id: "112",
          title: "Delete"
        }
      }
    },
    {
      customerNumber: 114,
      customerName: "Australian Collectors, Co.",
      contactLastName: "Ferguson",
      contactFirstName: "Peter",
      phone: "03 9520 4555",
      addressLine1: "636 St Kilda Road",
      addressLine2: "Level 3",
      city: "Melbourne",
      state: "Victoria",
      postalCode: "3004",
      country: "Australia",
      salesRepEmployeeNumber: 1611,
      creditLimit: 117300,
      avatar: "",
      photo: "",
      attach: "",
      sex: "male",
      interests: "",
      __actions__: {
        view: {
          id: "114",
          title: "View"
        },
        edit: {
          id: "114",
          title: "Edit"
        },
        delete: {
          id: "114",
          title: "Delete"
        }
      }
    },
    {
      customerNumber: 119,
      customerName: "La Rochelle Gifts",
      contactLastName: "Labrune",
      contactFirstName: "Janine ",
      phone: "40.67.8555",
      addressLine1: "67, rue des Cinquante Otages",
      addressLine2: null,
      city: "Nantes",
      state: null,
      postalCode: "44000",
      country: "France",
      salesRepEmployeeNumber: 1370,
      creditLimit: 118200,
      avatar: "",
      photo: "",
      attach: "",
      sex: "male",
      interests: "",
      __actions__: {
        view: {
          id: "119",
          title: "View"
        },
        edit: {
          id: "119",
          title: "Edit"
        },
        delete: {
          id: "119",
          title: "Delete"
        }
      }
    },
    {
      customerNumber: 121,
      customerName: "Baane Mini Imports",
      contactLastName: "Bergulfsen",
      contactFirstName: "Jonas ",
      phone: "07-98 9555",
      addressLine1: "Erling Skakkes gate 78",
      addressLine2: null,
      city: "Stavern",
      state: null,
      postalCode: "4110",
      country: "Norway",
      salesRepEmployeeNumber: 1504,
      creditLimit: 81700,
      avatar: "",
      photo: "",
      attach: "",
      sex: "male",
      interests: "",
      __actions__: {
        view: {
          id: "121",
          title: "View"
        },
        edit: {
          id: "121",
          title: "Edit"
        },
        delete: {
          id: "121",
          title: "Delete"
        }
      }
    },
    {
      customerNumber: 124,
      customerName: "Mini Gifts Distributors Ltd.",
      contactLastName: "Nelson",
      contactFirstName: "Susan",
      phone: "4155551450",
      addressLine1: "5677 Strong St.",
      addressLine2: null,
      city: "San Rafael",
      state: "CA",
      postalCode: "97562",
      country: "USA",
      salesRepEmployeeNumber: 1165,
      creditLimit: 210500,
      avatar: "",
      photo: "",
      attach: "",
      sex: "male",
      interests: "",
      __actions__: {
        view: {
          id: "124",
          title: "View"
        },
        edit: {
          id: "124",
          title: "Edit"
        },
        delete: {
          id: "124",
          title: "Delete"
        }
      }
    },
    {
      customerNumber: 125,
      customerName: "Havel & Zbyszek Co",
      contactLastName: "Piestrzeniewicz",
      contactFirstName: "Zbyszek ",
      phone: "(26) 642-7555",
      addressLine1: "ul. Filtrowa 68",
      addressLine2: null,
      city: "Warszawa",
      state: null,
      postalCode: "01-012",
      country: "Poland",
      salesRepEmployeeNumber: null,
      creditLimit: 0,
      avatar: "",
      photo: "",
      attach: "",
      sex: "male",
      interests: "",
      __actions__: {
        view: {
          id: "125",
          title: "View"
        },
        edit: {
          id: "125",
          title: "Edit"
        },
        delete: {
          id: "125",
          title: "Delete"
        }
      }
    },
    {
      customerNumber: 128,
      customerName: "Blauer See Auto, Co.",
      contactLastName: "Keitel",
      contactFirstName: "Roland",
      phone: "+49 69 66 90 2555",
      addressLine1: "Lyonerstr. 34",
      addressLine2: null,
      city: "Frankfurt",
      state: null,
      postalCode: "60528",
      country: "Germany",
      salesRepEmployeeNumber: 1504,
      creditLimit: 59700,
      avatar: "",
      photo: "",
      attach: "",
      sex: "male",
      interests: "",
      __actions__: {
        view: {
          id: "128",
          title: "View"
        },
        edit: {
          id: "128",
          title: "Edit"
        },
        delete: {
          id: "128",
          title: "Delete"
        }
      }
    },
    {
      customerNumber: 129,
      customerName: "Mini Wheels Co.",
      contactLastName: "Murphy",
      contactFirstName: "Julie",
      phone: "6505555787",
      addressLine1: "5557 North Pendale Street",
      addressLine2: null,
      city: "San Francisco",
      state: "CA",
      postalCode: "94217",
      country: "USA",
      salesRepEmployeeNumber: 1165,
      creditLimit: 64600,
      avatar: "",
      photo: "",
      attach: "",
      sex: "male",
      interests: "",
      __actions__: {
        view: {
          id: "129",
          title: "View"
        },
        edit: {
          id: "129",
          title: "Edit"
        },
        delete: {
          id: "129",
          title: "Delete"
        }
      }
    },
    {
      customerNumber: 131,
      customerName: "Land of Toys Inc.",
      contactLastName: "Lee",
      contactFirstName: "Kwai",
      phone: "2125557818",
      addressLine1: "897 Long Airport Avenue",
      addressLine2: null,
      city: "NYC",
      state: "NY",
      postalCode: "10022",
      country: "USA",
      salesRepEmployeeNumber: 1323,
      creditLimit: 114900,
      avatar: "",
      photo: "",
      attach: "",
      sex: "male",
      interests: "",
      __actions__: {
        view: {
          id: "131",
          title: "View"
        },
        edit: {
          id: "131",
          title: "Edit"
        },
        delete: {
          id: "131",
          title: "Delete"
        }
      }
    }
  ],
  pagination: {
    current_page: 1,
    first_page_url:
      "http://lara.pma.local/dbmanager-api/tables/customers/data?page=1",
    from: 1,
    last_page: 13,
    last_page_url:
      "http://lara.pma.local/dbmanager-api/tables/customers/data?page=13",
    next_page_url:
      "http://lara.pma.local/dbmanager-api/tables/customers/data?page=2",
    path: "http://lara.pma.local/dbmanager-api/tables/customers/data",
    per_page: 10,
    prev_page_url: null,
    to: 10,
    total: 122
  },
  columns: [
    {
      name: "customerNumber",
      type: "integer",
      default: null,
      notnull: true,
      length: null,
      precision: 10,
      scale: 0,
      fixed: false,
      unsigned: false,
      autoincrement: true,
      columnDefinition: null,
      comment: null,
      label: "CustomerNumber",
      field: "customerNumber",
      sortable: true,
      filterable: true,
      value: null,
      visible: true,
      style: []
    },
    {
      name: "customerName",
      type: "string",
      default: null,
      notnull: true,
      length: 50,
      precision: 10,
      scale: 0,
      fixed: false,
      unsigned: false,
      autoincrement: false,
      columnDefinition: null,
      comment: null,
      collation: "utf8_general_ci",
      label: "CustomerName",
      field: "customerName",
      sortable: true,
      filterable: true,
      value: null,
      visible: true,
      style: []
    },
    {
      name: "contactLastName",
      type: "string",
      default: null,
      notnull: true,
      length: 50,
      precision: 10,
      scale: 0,
      fixed: false,
      unsigned: false,
      autoincrement: false,
      columnDefinition: null,
      comment: null,
      collation: "utf8_general_ci",
      label: "ContactLastName",
      field: "contactLastName",
      sortable: true,
      filterable: true,
      value: null,
      visible: true,
      style: []
    },
    {
      name: "contactFirstName",
      type: "string",
      default: null,
      notnull: true,
      length: 50,
      precision: 10,
      scale: 0,
      fixed: false,
      unsigned: false,
      autoincrement: false,
      columnDefinition: null,
      comment: null,
      collation: "utf8_general_ci",
      label: "ContactFirstName",
      field: "contactFirstName",
      sortable: true,
      filterable: true,
      value: null,
      visible: true,
      style: []
    },
    {
      name: "phone",
      type: "string",
      default: null,
      notnull: true,
      length: 50,
      precision: 10,
      scale: 0,
      fixed: false,
      unsigned: false,
      autoincrement: false,
      columnDefinition: null,
      comment: null,
      collation: "utf8_general_ci",
      label: "Phone",
      field: "phone",
      sortable: true,
      filterable: true,
      value: null,
      visible: true,
      style: []
    },
    {
      name: "addressLine1",
      type: "string",
      default: null,
      notnull: true,
      length: 50,
      precision: 10,
      scale: 0,
      fixed: false,
      unsigned: false,
      autoincrement: false,
      columnDefinition: null,
      comment: null,
      collation: "utf8_general_ci",
      label: "AddressLine1",
      field: "addressLine1",
      sortable: true,
      filterable: true,
      value: null,
      visible: true,
      style: []
    },
    {
      name: "addressLine2",
      type: "string",
      default: null,
      notnull: false,
      length: 50,
      precision: 10,
      scale: 0,
      fixed: false,
      unsigned: false,
      autoincrement: false,
      columnDefinition: null,
      comment: null,
      collation: "utf8_general_ci",
      label: "AddressLine2",
      field: "addressLine2",
      sortable: true,
      filterable: true,
      value: null,
      visible: true,
      style: []
    },
    {
      name: "city",
      type: "string",
      default: null,
      notnull: true,
      length: 50,
      precision: 10,
      scale: 0,
      fixed: false,
      unsigned: false,
      autoincrement: false,
      columnDefinition: null,
      comment: null,
      collation: "utf8_general_ci",
      label: "City",
      field: "city",
      sortable: true,
      filterable: true,
      value: null,
      visible: true,
      style: []
    },
    {
      name: "state",
      type: "string",
      default: null,
      notnull: false,
      length: 50,
      precision: 10,
      scale: 0,
      fixed: false,
      unsigned: false,
      autoincrement: false,
      columnDefinition: null,
      comment: null,
      collation: "utf8_general_ci",
      label: "State",
      field: "state",
      sortable: true,
      filterable: true,
      value: null,
      visible: true,
      style: []
    },
    {
      name: "postalCode",
      type: "string",
      default: null,
      notnull: false,
      length: 15,
      precision: 10,
      scale: 0,
      fixed: false,
      unsigned: false,
      autoincrement: false,
      columnDefinition: null,
      comment: null,
      collation: "utf8_general_ci",
      label: "PostalCode",
      field: "postalCode",
      sortable: true,
      filterable: true,
      value: null,
      visible: true,
      style: []
    },
    {
      name: "country",
      type: "string",
      default: null,
      notnull: true,
      length: 50,
      precision: 10,
      scale: 0,
      fixed: false,
      unsigned: false,
      autoincrement: false,
      columnDefinition: null,
      comment: null,
      collation: "utf8_estonian_ci",
      label: "Country",
      field: "country",
      sortable: true,
      filterable: true,
      value: null,
      visible: true,
      style: []
    },
    {
      name: "salesRepEmployeeNumber",
      type: "integer",
      default: null,
      notnull: false,
      length: null,
      precision: 10,
      scale: 0,
      fixed: false,
      unsigned: false,
      autoincrement: false,
      columnDefinition: null,
      comment: null,
      label: "SalesRepEmployeeNumber",
      field: "salesRepEmployeeNumber",
      sortable: true,
      filterable: true,
      value: null,
      visible: true,
      style: []
    },
    {
      name: "creditLimit",
      type: "float",
      default: null,
      notnull: false,
      length: 0,
      precision: 10,
      scale: 0,
      fixed: false,
      unsigned: false,
      autoincrement: false,
      columnDefinition: null,
      comment: null,
      label: "CreditLimit",
      field: "creditLimit",
      sortable: true,
      filterable: true,
      value: null,
      visible: true,
      style: []
    },
    {
      name: "avatar",
      type: "blob",
      default: null,
      notnull: true,
      length: 16777215,
      precision: 10,
      scale: 0,
      fixed: false,
      unsigned: false,
      autoincrement: false,
      columnDefinition: null,
      comment: null,
      label: "Avatar",
      field: "avatar",
      sortable: true,
      filterable: true,
      value: null,
      visible: true,
      style: []
    },
    {
      name: "photo",
      type: "string",
      default: null,
      notnull: true,
      length: 50,
      precision: 10,
      scale: 0,
      fixed: false,
      unsigned: false,
      autoincrement: false,
      columnDefinition: null,
      comment: null,
      collation: "utf8_general_ci",
      label: "Photo",
      field: "photo",
      sortable: true,
      filterable: true,
      value: null,
      visible: true,
      style: []
    },
    {
      name: "attach",
      type: "string",
      default: null,
      notnull: true,
      length: 255,
      precision: 10,
      scale: 0,
      fixed: false,
      unsigned: false,
      autoincrement: false,
      columnDefinition: null,
      comment: null,
      collation: "utf8_general_ci",
      label: "Attach",
      field: "attach",
      sortable: true,
      filterable: true,
      value: null,
      visible: true,
      style: []
    },
    {
      name: "sex",
      type: "enum",
      default: null,
      notnull: true,
      length: 0,
      precision: 10,
      scale: 0,
      fixed: false,
      unsigned: false,
      autoincrement: false,
      columnDefinition: null,
      comment: null,
      collation: "utf8_general_ci",
      options: ["male", "female"],
      label: "Sex",
      field: "sex",
      sortable: true,
      filterable: true,
      value: null,
      visible: true,
      style: []
    },
    {
      name: "interests",
      type: "set",
      default: null,
      notnull: true,
      length: 0,
      precision: 10,
      scale: 0,
      fixed: false,
      unsigned: false,
      autoincrement: false,
      columnDefinition: null,
      comment: null,
      collation: "utf8_general_ci",
      options: [
        "sports",
        "programming",
        "cars",
        "girls",
        "drinks",
        "fights",
        "history",
        "cooking",
        "shopping"
      ],
      label: "Interests",
      field: "interests",
      sortable: true,
      filterable: true,
      value: null,
      visible: true,
      style: []
    },
    {
      label: "__actions__",
      field: "__actions__",
      sortable: false,
      filterable: false,
      value: null,
      visible: true,
      style: {
        width: "160px"
      }
    }
  ],
  meta: {
    table_catalog: "def",
    table_schema: "rad_crud",
    table_name: "customers",
    table_type: "BASE TABLE",
    engine: "InnoDB",
    version: 10,
    row_format: "Dynamic",
    table_rows: 122,
    avg_row_length: 537,
    data_length: 65536,
    max_data_length: 0,
    index_length: 0,
    data_free: 0,
    auto_increment: 497,
    create_time: "2018-11-14 11:15:22",
    update_time: null,
    check_time: null,
    table_collation: "utf8_general_ci",
    checksum: null,
    create_options: "",
    table_comment: ""
  }
};
export const BaseTableStory = () => {
  return (
    <Router>
      <Table long {...defaultProps} table={table} rowKey={table.keyField} />
    </Router>
  );
};
