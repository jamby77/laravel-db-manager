import React from "react";
import EntityForm from "../../resources/js/components/crud/forms/EntityForm";

export const PlainFormStory = () => {
  const row = {};
  const config = [
    {
      id: "text",
      name: "text",
      label: "Text field",
      placeholder: "Text field",
      default: "",
      type: "text"
    },
    {
      id: "select",
      name: "select",
      label: "Plain select field",
      default: "two",
      type: "select",
      options: ["one", "two", "three"]
    }
  ];

  const errors = { text: "Text field error" };
  return (
    <EntityForm
      handleSubmit={(values, actions) => {
        console.log("handle submit", values, actions);
      }}
      submitBinder={fn => {
        console.log("binding submit action", fn);
      }}
      entity={row}
      errors={errors}
      formConfig={config}
    />
  );
};
