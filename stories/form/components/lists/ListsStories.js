import React from "react";
import { prepareSelectOptions } from "../../../../resources/js/misc/forms";
import MultiSelect from "../../../../resources/js/components/crud/forms/MultiSelect";
import BaseSelect from "../../../../resources/js/components/crud/forms/BaseSelect";
import MultiSelectionItem from "../../../../resources/js/components/crud/forms/select-components/MultiSelectionItem";
import MultiSelectionAddon from "../../../../resources/js/components/crud/forms/select-components/MultiSelectionAddon";
import MultiSelectSelectionFormInput from "../../../../resources/js/components/crud/forms/select-components/MultiSelectSelectionFormInput";
import Downshift from "downshift";
import MultiSelectionMenu from "../../../../resources/js/components/crud/forms/select-components/MultiSelectionMenu";
import MultiSelectionSearchInput from "../../../../resources/js/components/crud/forms/select-components/MultiSelectionSearchInput";

export const SimpleList = () => {
  return (
    <div style={{ width: "30%" }}>
      <div style={{ width: "80%" }}>
        <MultiSelect
          isOpen
          items={prepareSelectOptions(["one", "two", "three", "four"])}
          placeholder="Choose a few"
          values={prepareSelectOptions(["one", "three"])}
        />
      </div>
      <BaseSelect
        items={["one", "two", "three", "four"]}
        placeholder="Choose a count"
        value={"four"}
      />
      <p>Some sample text</p>
    </div>
  );
};

export const LongListStory = () => {
  const items = [];
  for (let i = 0; i < 1000; i++) {
    items.push(`item${i}`);
  }

  const multiItems = [items[0], items[1], items[2], items[3]];
  return (
    <div style={{ width: "30%" }}>
      <BaseSelect
        items={items}
        placeholder="Choose a count"
        value={items[Math.floor(Math.random() * Math.floor(1000))]}
      />

      <MultiSelect
        items={items}
        placeholder="Choose a few"
        values={multiItems}
      />
    </div>
  );
};

export const MultiSelectItemStory = () => {
  const item = { id: 1, text: "One" };
  const handleClick = id => console.log(id, id === item.id);
  return (
    <div style={{ width: "100px" }}>
      <MultiSelectionItem
        id={item.id}
        label={item.text}
        onClick={handleClick}
      />
    </div>
  );
};

export const MultiSelectItemAddonStory = () => {
  const ref = React.createRef();
  return (
    <div style={{ width: "100px" }}>
      <MultiSelectionAddon
        innerRef={ref}
        numItems={23}
        onClick={e => console.log({ ...e })}
      />
    </div>
  );
};

export const MultiSelectRowStory = () => {
  const items = [
    { id: 1, text: "One" },
    { id: 2, text: "two" },
    { id: 3, text: "three" },
    { id: 4, text: "four" }
  ];
  return [
    <div key={300} style={{ width: "300px" }}>
      <MultiSelectSelectionFormInput items={items} />
    </div>,
    <div key={400} style={{ width: "400px" }}>
      <MultiSelectSelectionFormInput items={items} />
    </div>,
    <div key={500} style={{ width: "500px" }}>
      <MultiSelectSelectionFormInput items={items} />
    </div>
  ];
};

export const MultiSelectMenuStory = () => {
  const items = [
    { id: 1, text: "One" },
    { id: 2, text: "two" },
    { id: 3, text: "three" },
    { id: 4, text: "four" }
  ];
  const selection = [{ id: 2, text: "two" }, { id: 3, text: "three" }];
  const itemToString = i => i.text;
  return (
    <Downshift itemToString={itemToString}>
      {downshift => {
        return (
          <div style={{ width: "200px", position: "relative" }}>
            <MultiSelectionMenu
              downshift={downshift}
              isOpen={true}
              items={items}
              selection={selection}
              itemToString={itemToString}
              compareItems={(a, b) => {
                return a.id === b.id;
              }}
            />
          </div>
        );
      }}
    </Downshift>
  );
};

export const MultiSelectSearchInputStory = () => {
  const itemToString = i => i;
  return (
    <Downshift itemToString={itemToString}>
      {downshift => {
        return (
          <div style={{ width: "200px", position: "relative" }}>
            <MultiSelectionSearchInput
              isOpen={true}
              downshift={downshift}
              itemToString={itemToString}
            />
          </div>
        );
      }}
    </Downshift>
  );
};
export const MultiSelectMenuWithSearchStory = () => {
  const items = [
    { id: 1, text: "One" },
    { id: 2, text: "two" },
    { id: 3, text: "three" },
    { id: 4, text: "four" }
  ];
  const selection = [{ id: 2, text: "two" }, { id: 3, text: "three" }];
  const itemToString = i => (i && i.text ? i.text : i);
  return (
    <Downshift
      itemToString={itemToString}
      onChange={args => console.log(args)}
      onInputValueChange={args => console.log(args)}
    >
      {downshift => {
        return (
          <div style={{ width: "200px", position: "relative" }}>
            <MultiSelectionMenu
              downshift={downshift}
              isOpen
              searchable
              items={items}
              selection={selection}
              itemToString={itemToString}
              compareItems={(a, b) => {
                return a.id === b.id;
              }}
            />
          </div>
        );
      }}
    </Downshift>
  );
};
