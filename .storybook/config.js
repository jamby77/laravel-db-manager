import { configure, addDecorator } from "@storybook/react";
import { withConsole } from "@storybook/addon-console";
import {
  configureViewport,
  INITIAL_VIEWPORTS
} from "@storybook/addon-viewport";
import "@storybook/addon-console";

configureViewport({
  viewports: { ...INITIAL_VIEWPORTS }
});
// automatically import all files ending in *.stories.js
const req = require.context("../stories", true, /.stories\.js$/);
function loadStories() {
  req.keys().forEach(filename => req(filename));
}

addDecorator((storyFn, context) => withConsole()(storyFn)(context));

configure(loadStories, module);
